//
//  AppDelegate.swift
//  MasBoletos
//
//  Created by Javier Morales on 7/23/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //UserDefaults.standard.removeUser()
        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: "AQWaM1shY74BZEcZITi7dr2VFA6PJpp9ucaMSeM4pz0oVLINOW1tw-qibp5F9uXyRSCqBfKw7qDNpHJW",PayPalEnvironmentSandbox: "Ad99nJUoz4m53PDErQYwCo9MM0DZdvJ0UKg6V1ZsQYrDxBGp5d4GUInwHNqsLA7niTHjwQDuWiTqMuAn"])
        
        return true
        
    }

    func applicationWillResignActive(_ application: UIApplication) { }

    func applicationDidEnterBackground(_ application: UIApplication) { }

    func applicationWillEnterForeground(_ application: UIApplication) { }

    func applicationDidBecomeActive(_ application: UIApplication) { }

    func applicationWillTerminate(_ application: UIApplication) { }


}

