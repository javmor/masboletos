//
//  ModelSeccion.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/17/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

struct ModelSeccion {
    let _idZona: Int
    let _nombre: String
    let _numerado: Int
    
    init(_idZona: Int, _nombre: String, _numerado: Int) {
        self._idZona = _idZona
        self._nombre = _nombre
        self._numerado = _numerado
    }
}
