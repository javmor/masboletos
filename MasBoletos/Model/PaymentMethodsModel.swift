//
//  PaymentMethodsModel.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 3/17/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

struct PaymentMethodsModel {
    let idTipoPago: Int
    let porcentajeCargo: Int
    let texto: String
    let importeFijo: Int
    
    init(idTipoPago: Int, porcentajeCargo: Int, texto: String, importeFijo: Int) {
        self.idTipoPago = idTipoPago
        self.porcentajeCargo = porcentajeCargo
        self.texto = texto
        self.importeFijo = importeFijo
    }
}


struct DeliveryMethodModel {
    let idforma: Int
    let porcentajecargo: Int
    let costo: String
    let texto: String
    let descripcion: String
    
    init(idforma: Int, porcentajecargo: Int, costo: String, texto: String, descripcion: String) {
        self.idforma = idforma
        self.porcentajecargo = porcentajecargo
        self.costo = costo
        self.texto = texto
        self.descripcion = descripcion
    }
}

struct PaymenthModel: Decodable {
    let Tipo: String
    let IdTipoPago: Int
    let porcentajecargo: Int
    let ImporteFijo: Int
    let texto: String
    let descripcion: String
}
