//
//  ModelFuncionesEventos.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 12/29/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

struct ModelFuncionesEventos {
    let idevento_funcion: Int
    let FechaLarga: String
    let hora: String
    let fecha_completa: String
    
    init(idevento_funcion: Int, FechaLarga: String, hora: String, fecha_completa: String) {
        self.idevento_funcion = idevento_funcion
        self.FechaLarga = FechaLarga
        self.hora = hora
        self.fecha_completa = fecha_completa
    }
}
