//
//  ModelEventoEncabezado.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 12/29/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

struct ModelEventoEncabezado {
    let imagen: String
    let direccion: String
    let evento: String
    let lugar: String
    let FechaLarga: String
    let hora: String
    let descripcion: String
    let EventoMapa: String
    
    init(imagen: String, direccion: String, evento: String, lugar: String, FechaLarga: String, hora: String, descripcion: String, EventoMapa: String) {
        
        self.imagen = imagen
        self.direccion = direccion
        self.evento = evento
        self.lugar = lugar
        self.FechaLarga = FechaLarga
        self.hora = hora
        self.descripcion = descripcion
        self.EventoMapa = EventoMapa
    }
}
