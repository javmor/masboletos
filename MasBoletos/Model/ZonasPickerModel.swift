//
//  ZonasPickerModel.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 1/30/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

import Foundation


struct ZonasPickerModel {
    let precio: Int
    let grupo: String
    let disponibilidad: Int
    let EventoMapam: String
    let numerado: Int
    
    init(precio: Int, grupo: String, disponibilidad: Int, EventoMapam: String, numerado: Int) {
        self.precio = precio
        self.grupo = grupo
        self.disponibilidad = disponibilidad
        self.EventoMapam = EventoMapam
        self.numerado = numerado
    }
    
}
