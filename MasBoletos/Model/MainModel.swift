//
//  MainModel.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 12/16/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import Foundation

struct IndividualesModel: Decodable {
    let evento: String
    let imagen: String
    let respuesta: Int
    let imagencarrusel: String
    let idevento: Int
    let eventogrupo: Int
}

struct PatrocinadoresModel: Decodable {
    
}
