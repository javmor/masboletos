//
//  ModelLogin.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 4/28/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

import Foundation


struct Login: Decodable {
    let respuesta: Bool
    let mensaje: String
    let id_cliente: Int
    let usuario: String
    let tipousuario: String
    let pleca: String
    let acc_org: String
}
