//
//  OrganizadoresModel.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/11/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

struct PaquetesOrganizadoresModel {
    let idorganizador: Int
    let nombre: String
    let banner: String
    let domicilio: String
    
    init(idorganizador: Int, nombre: String, banner: String, domicilio: String) {
        self.idorganizador = idorganizador
        self.nombre = nombre
        self.banner = banner
        self.domicilio = domicilio
    }
}
