//
//  PaquetesZonasModel.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/18/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

struct PaquetesZonasModel {
    let zona: String
    let numerado: Int
    let comision: String
    let precio: String
    
    init(zona: String, numerado: Int, comision: String, precio: String) {
        self.zona = zona
        self.numerado = numerado
        self.comision = comision
        self.precio = precio
    }
}
