//
//  PaquetesSeccionModel.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/18/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

struct PaquetesSeccionModel {
    let value: Int
    let descripcion: String
    
    init(value: Int, descripcion: String) {
        self.value = value
        self.descripcion = descripcion
    }
}
