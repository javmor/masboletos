//
//  MejorDisponiblePaquete.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 3/10/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

import Foundation

struct ModelMejorDisponiblePaquete {
    var mensagesetTipo: Int
    var mensagesetMensage: String
    var mensagesetDescripcion: String
    var mensagesetAsientos: String
    var mensagesetIdZona: Int
    var disponibles: String
    var mensagesetIdPuerta: Int
    var aforo: String
    var vendidos: String
    var precio: String
    var idevento: Int
    var mensagesetFila: String
    var mensagesetImporteComision: Int
    var mensagesetNumerado: Int
    var mensagesetIniColumna: Int
    var mensagesetFinColumna: Int
    var idfila: Int
    
    init(mensagesetTipo: Int, mensagesetMensage: String, mensagesetDescripcion: String, mensagesetAsientos: String,
        mensagesetIdZona: Int, disponibles: String, mensagesetIdPuerta: Int, aforo: String,
        vendidos: String, precio: String, idevento: Int, mensagesetFila: String, mensagesetImporteComision: Int,
        mensagesetNumerado: Int, mensagesetIniColumna: Int, mensagesetFinColumna: Int, idfila: Int) {
        
        self.mensagesetTipo = mensagesetTipo
        self.mensagesetMensage = mensagesetMensage
        self.mensagesetDescripcion = mensagesetDescripcion
        self.mensagesetAsientos = mensagesetAsientos
        self.mensagesetIdZona = mensagesetIdZona
        self.disponibles = disponibles
        self.mensagesetIdPuerta = mensagesetIdPuerta
        self.aforo = aforo
        self.vendidos = vendidos
        self.precio = precio
        self.idevento = idevento
        self.mensagesetFila = mensagesetFila
        self.mensagesetImporteComision = mensagesetImporteComision
        self.mensagesetNumerado = mensagesetNumerado
        self.mensagesetIniColumna = mensagesetIniColumna
        self.mensagesetFinColumna = mensagesetFinColumna
        self.idfila = idfila
        
    }
}
