//
//  PaquetesMuestraModel.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/13/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//
struct PaquetesMuestraModel {
    let imagen: String?
    let nombre: String
    let precio: String
    let idEventPack: Int
    
    init(imagen: String, nombre: String, precio: String, idEventPack: Int) {
        self.imagen = imagen
        self.nombre = nombre
        self.precio = precio
        self.idEventPack = idEventPack
    }
    
}
