//
//  UserEventsModel.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/2/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

struct UserEventsModel {
    let imagen: String
    let cantidad: Int
    let evento: String
    let fecha_evento: String
    let estatus: String
    let idForma: Int
    let transaccion: String
    
    init(imagen: String, cantidad: Int, evento: String, fecha_evento: String, estatus: String, idForma: Int, transaccion: String) {
        self.imagen = imagen
        self.cantidad = cantidad
        self.evento = evento
        self.fecha_evento = fecha_evento
        self.estatus = estatus
        self.idForma = idForma
        self.transaccion = transaccion
    }
}
