//
//  PatrocinadoresModel.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/12/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

struct Patrocinadores {
    let id_organizador: Int
    let nombre: String
    let banner: String
    
    init(id_organizador: Int, nombre: String, banner: String) {
        self.id_organizador = id_organizador
        self.nombre = nombre
        self.banner = banner
    }
}
