//
//  ModelMejorDisponible.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/16/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

struct ModelMejorDisponible {
    let mensagesetTipo: Int
    let mensagesetMensage: String
    let mensagesetIdZona: Int
    let mensagesetFila: String
    let mensagesetIniColumna: Int
    let mensagesetFinColumna: Int
    let idfila: Int
    let mensagesetAsientos: String
    let mensagesetDescripcion: String
    
    init(mensagesetTipo: Int, mensagesetMensage: String, mensagesetIdZona: Int, mensagesetFila: String, mensagesetIniColumna: Int, mensagesetFinColumna: Int, idfila: Int, mensagesetAsientos: String, mensagesetDescripcion: String) {
        
        self.mensagesetTipo = mensagesetTipo
        self.mensagesetMensage = mensagesetMensage
        self.mensagesetIdZona = mensagesetIdZona
        self.mensagesetFila = mensagesetFila
        self.mensagesetIniColumna = mensagesetIniColumna
        self.mensagesetFinColumna = mensagesetFinColumna
        self.idfila = idfila
        self.mensagesetAsientos = mensagesetAsientos
        self.mensagesetDescripcion = mensagesetDescripcion
    }
}
