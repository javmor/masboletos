////
////  Timer.swift
////  MasBoletos
////
////  Created by Victor Javier Arroyo Morales on 2/27/19.
////  Copyright © 2019 IT-STAM. All rights reserved.
////
//
//import Foundation
//
//struct Countdown {
//
//    // Anchor time
//    let startTime: Date = Date()
//    // The total amount of time to wait
//    let duration: TimeInterval = 200 * 60 // 200 minutes
//
//
//    let formatter = DateComponentsFormatter()
//    //let formatter = DateComponentsFormatter()
//    formatter.allowedUnits = [.hour, .minute, .second]
//    formatter.zeroFormattingBehavior = .dropLeading
//    formatter.unitsStyle = .short
//    // The amount of time which has past since we started
//    var runningTime: TimeInterval = 0
//
//    // This is just so I can atrificially update the time
//    var time: Date = Date()
//    let cal: Calendar = Calendar.current
//    repeat {
//        // Simulate the passing of time, by the minute
//        // If this was been called from a timer, then you'd
//        // simply use the current time
//        time = cal.date(byAdding: .minute, value: 1, to: time)!
//
//        // How long have we been running for?
//        runningTime = time.timeIntervalSince(startTime)
//        // Have we run out of time?
//        if runningTime < duration {
//            // Print the amount of time remaining
//            print(formatter.string(from: duration - runningTime)!)
//        }
//    } while runningTime < duration
//}
