//
//  UILabel.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 5/1/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

import UIKit

extension UILabel {
    convenience init(text: String?, font: UIFont? = UIFont.systemFont(ofSize: 14),textColor: UIColor = .black, textAlignment: NSTextAlignment = .left, numberOfLines: Int = 1) {
        self.init()
        self.text = text
        self.textColor = textColor
        self.textAlignment = textAlignment
        self.numberOfLines = numberOfLines
    }
}
