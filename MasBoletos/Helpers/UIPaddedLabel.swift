//
//  EdgeInsetLabel.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 10/15/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit

public class UIPaddedLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 0.0
    @IBInspectable var leftInset: CGFloat = 0.0
    @IBInspectable var rightInset: CGFloat = 0.0
    
    public override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    public override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
    
    override public func sizeToFit() {
        super.sizeThatFits(intrinsicContentSize)
    }
}
