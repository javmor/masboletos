//
//  Services.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 4/2/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

import Alamofire

struct Service {

    static let shared = Service()
    
    func fetchGenericData<T: Decodable>(urlString: String, params: [String:String]? = nil, headers: [String:String]? = nil, method: HTTPMethod, completion: @escaping (T) -> ()) {
        Alamofire.request(urlString, method: method, parameters: params, encoding: URLEncoding.httpBody, headers: headers).validate().responseJSON { response in
            
            switch response.result {
            case .success:
                guard let data = response.data else { return }
                do {
                    let feed = try JSONDecoder().decode(T.self, from: data)
                    completion(feed)
                    
                }catch let jsonErr {
                    print("Failed to serialize json: ", jsonErr)
                }
                
            case .failure(let error):
                print("Failed to fetch: ", error)
            }
        }
    }
    
}
