//
//  EventosCell.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/2/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

import UIKit

class EventosCell: UITableViewCell {
    
    
    @IBOutlet weak var titleEvento: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
