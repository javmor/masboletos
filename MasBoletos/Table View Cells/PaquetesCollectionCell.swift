//
//  PaquetesCell.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/11/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//
import UIKit

class PaquetesCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var imgEvento: UIImageView!
    @IBOutlet weak var name_evento: UILabel!
}
