//
//  EventosPaquetesCell.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/13/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

import UIKit

class EventosPaquetesCell: UICollectionViewCell {
    @IBOutlet weak var imgEvento: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var showEventos: UIButton!
    @IBOutlet weak var cellView: UIView!
}
