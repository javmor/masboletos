//
//  ProfileTableViewCell.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 10/11/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit

class BoletosTableViewCell: UITableViewCell {


    @IBOutlet weak var countTickets: UILabel!
    @IBOutlet weak var imgEvento: UIImageView!
    @IBOutlet weak var dateEvento: UILabel!
    @IBOutlet weak var estatusEvento: UILabel!
    @IBOutlet weak var viewCell: UIView!
    
}
