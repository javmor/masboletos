//
//  PaymentsTableViewCell.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 9/10/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit

class PaymentsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewPayment: UIView!
    @IBOutlet weak var labelPayment: UILabel!
    
}
