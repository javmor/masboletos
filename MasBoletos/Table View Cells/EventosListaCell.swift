//
//  EventosAdentro.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/14/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

import UIKit

class EventosListaCell: UICollectionViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var imgEvento: UIImageView!
    @IBOutlet weak var title: UILabel!
}
