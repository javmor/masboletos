//
//  CarouselCollectionViewCell.swift
//  MasBoletos
//
//  Created by Javier Morales on 7/23/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit

class CarouselCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var itemImage: UIImageView!
    
}
