//
//  InfoEvent.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 5/15/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//
import SDWebImage

class InfoEvent: UIView {
    
    let imgBackgroundView: UIImageView = {
        let img = UIImageView()
        img.contentMode = ContentMode.scaleAspectFit
        img.image = #imageLiteral(resourceName: "logo_masboletos")
        img.blurImage()
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let eventoImg: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.image = #imageLiteral(resourceName: "oxxo")
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.lineBreakMode = NSLineBreakMode.byWordWrapping
        lbl.numberOfLines = 3
        //lbl.backgroundColor = .blue
        return lbl
    }()
    
    let directionLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 3
        lbl.textColor = .white
        //lbl.backgroundColor = .red
        return lbl
    }()
    
    let moreInformationLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        //lbl.backgroundColor = .yellow
        return lbl
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layer.cornerRadius = 8
        setupShadow(opacity: 0.05, radius: 0, offset: .init(width: 0, height: 6), color: .black)
        setup()
    }
    
    fileprivate func setup() {
        
        addSubview(imgBackgroundView)
        imgBackgroundView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, size: .init(width: 0, height: 150))
        
        let darkBlur = UIBlurEffect(style: .light)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = imgBackgroundView.bounds
        imgBackgroundView.addSubview(blurView)
        
        let viewClear = UIView()
        viewClear.backgroundColor = .clear
        
        imgBackgroundView.addSubview(viewClear)
        viewClear.fillSuperview()
        
        [titleLabel, directionLabel, moreInformationLabel, eventoImg].forEach { viewClear.addSubview($0) }
        
        titleLabel.anchor(top: imgBackgroundView.topAnchor, leading: nil, bottom: nil, trailing: imgBackgroundView.trailingAnchor, padding: .init(top: 8, left: 0, bottom: 0, right: 8), size: .init(width: 200, height: 50))
        
        directionLabel.anchor(top: titleLabel.bottomAnchor, leading: nil, bottom: nil, trailing: titleLabel.trailingAnchor, padding: .init(top: 8, left: 0, bottom: 0, right: 0))
        directionLabel.widthAnchor.constraint(equalTo: titleLabel.widthAnchor).isActive = true
        directionLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        moreInformationLabel.anchor(top: directionLabel.bottomAnchor, leading: nil, bottom: nil, trailing: titleLabel.trailingAnchor, padding: .init(top: 8, left: 0, bottom: 0, right: 0))
        moreInformationLabel.widthAnchor.constraint(equalTo: titleLabel.widthAnchor).isActive = true
        moreInformationLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        eventoImg.anchor(top: titleLabel.topAnchor, leading: leadingAnchor, bottom: moreInformationLabel.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 8, bottom: 0, right: 0), size: .init(width: 150, height: 0))

    }
    
    func setupLabels(title: String? = "", direction: String? = "", moreInformation: String? = "", type: Int) {
        
        switch type {
        case 1:
            setupLabelsInfoEvent(label: titleLabel, title: title!)
            titleLabel.adjustsFontSizeToFitWidth = true
        case 2:
            //directionLabel.text = direction
            setupLabelsInfoEvent(label: directionLabel, title: direction!)
            directionLabel.adjustsFontSizeToFitWidth = true
        case 3:
            setupLabelsInfoEvent(label: moreInformationLabel, title: moreInformation!)
        default:
            print("je")
        }
    }
    
    fileprivate func setupLabelsInfoEvent(label: UILabel, title: String) {
        let attributedText = NSMutableAttributedString(attributedString: NSAttributedString(string: "\(title)  ", attributes: [NSAttributedString.Key.font : UIFont(name: "AvenirNext-Regular", size: 14) ?? UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.white]))
            label.attributedText = attributedText
    }
    
    func setupImages(imageUrlBackground: URL, imageUrl: URL) {
        imgBackgroundView.sd_setImage(with: imageUrlBackground, placeholderImage: #imageLiteral(resourceName: "logo_masboletos"))
        eventoImg.sd_setImage(with: imageUrl, placeholderImage: #imageLiteral(resourceName: "logo_masboletos"))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension UIImageView{
    func blurImage() {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame.size = self.frame.size
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
    }
}
extension UILabel {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font!], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}

