//
//  CreditsVC.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 10/12/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit

class CreditsVC: UIViewController {

    @IBOutlet weak var lblDevelopment: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var lblVersion: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Acerca de"
        lblDevelopment.layer.addBorder(edge: UIRectEdge.bottom, color: UIColor.gray, thickness: 1.0)
        lblAddress.layer.addBorder(edge: UIRectEdge.top, color: UIColor.gray, thickness: 1.0)
        btnEmail.layer.addBorder(edge: UIRectEdge.top, color: UIColor.gray, thickness: 1.0)
        lblVersion.text = "Versión: \(getAppInfo())"
    }
    
    @IBAction func btnTerms(_ sender: Any) {
        if let requestUrl = URL(string: "https://www.masboletos.mx/politicascompra.php") {
            UIApplication.shared.open(requestUrl as URL, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func btnAvisoDePrivacidad(_ sender: Any) {
        if let requestUrl = URL(string: "https://www.masboletos.mx/politicascompra.php") {
            UIApplication.shared.open(requestUrl as URL, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func btnSendEmail(_ sender: Any) {
        
        let email = "masboletos.aplicaciones@gmail.com"
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    private func getAppInfo()->String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        //let build = dictionary["CFBundleVersion"] as! String
        return version
    }
    
    private func getAppName()->String {
        let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as! String
        return appName
    }
    private func getOSInfo()->String {
        let os = ProcessInfo().operatingSystemVersion
        return String(os.majorVersion) + "." + String(os.minorVersion) + "." + String(os.patchVersion)
    }
    
}

