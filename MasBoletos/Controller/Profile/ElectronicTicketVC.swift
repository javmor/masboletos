//
//  ElectronicTicketVC.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 10/15/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit
import Alamofire

class ElectronicTicketVC: UIViewController {
    
    @IBOutlet weak var name_eventolbl: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lblHora: UILabel!
    @IBOutlet weak var lbl_transaccion: UILabel!
    @IBOutlet weak var imgQRCode: UIImageView!
    
    var jsonArray: NSArray?
    var transaccion = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Detalles del evento"
        let imageQR = generateQRCode(from: transaccion)

        name_eventolbl.adjustsFontSizeToFitWidth = true
        downloadInfoBuy()
        imgQRCode.image = imageQR
        
    }
    
    private func downloadInfoBuy() {
        let urlString = "https://www.masboletos.mx/appMasboletos.fueralinea/getInfoPorTransaccion.php?transaccion=\(transaccion)"
        
        
        Alamofire.request(urlString).responseJSON { response in
            if let JSON = response.result.value {
                
            
                self.jsonArray = JSON as? NSArray
                
                for item in self.jsonArray! as! [NSDictionary] {
                    
                    let name = item["evento"] as! String
                    self.name_eventolbl.text = "\(name)"
                    
                    let fechita = item["fechita"] as! String
                    self.lbldate.text = "\(fechita)"
                    
                    let hora = item["hora"] as! String
                    self.lblHora.text = "\(hora)"
                    
                    let folio = item["folio"] as! String
                    self.lbl_transaccion.text = "\(folio)"
                    
                }
            }
        }
        
        
    }
    
    private func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            
            guard let qrCodeImage = filter.outputImage else { return nil }
            
            let scaleX = imgQRCode.frame.size.width / qrCodeImage.extent.size.width
            let scaleY = imgQRCode.frame.size.height / qrCodeImage.extent.size.height
            
            let transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
            //let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
            
            
        }
        
        return nil
    }
    
}
