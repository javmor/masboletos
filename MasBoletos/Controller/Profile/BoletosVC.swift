//
//  BoletosVC.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 10/14/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit
import Alamofire

class BoletosVC: UIViewController {
    
    let tvEventos: UITableView = {
        let tv = UITableView()
        return tv
    }()
    
    let identifier = "CellEventos"
    
    let array_eventos = ["Mis eventos", "Mis eventos pasados"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        title = "Mis Boletos"
        view.backgroundColor = UIColor.colorWithHexString(hexStr: "F6F6F6")
    }
    
    private func setup() {
        tvEventos.register(UINib(nibName: "CellEventos", bundle: nil), forCellReuseIdentifier: identifier)
        tvEventos.separatorStyle = .none
        //tvEventos.backgroundColor = UIColor.colorWithHexString(hexStr: "F6F6F6")
        tvEventos.backgroundColor = UIColor.clear
        tvEventos.allowsMultipleSelection = false
        tvEventos.allowsSelection = true
        tvEventos.alwaysBounceVertical = false
        view.addSubview(tvEventos)
        tvEventos.fillSuperview()
        tvEventos.delegate = self
        tvEventos.dataSource = self
    }

}

extension BoletosVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_eventos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.identifier, for: indexPath) as! EventosCell
        cell.titleEvento.text = "\(array_eventos[indexPath.row])"
        cell.contentView.backgroundColor = UIColor.clear
        cell.cellView.setCardView(view: cell.cellView)
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let eventosVC = storyboard.instantiateViewController(withIdentifier: "Eventos") as! EventosVC
        self.navigationController?.pushViewController(eventosVC, animated: true)
        eventosVC.isEventsOrPastEvents = indexPath.row
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
