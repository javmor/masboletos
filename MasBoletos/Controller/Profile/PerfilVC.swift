//
//  PerfilVC.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 10/10/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit

class PerfilVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {

    @IBOutlet weak var viewProfileChange: UIView!
    @IBOutlet weak var tableViewProfile: UITableView!
    
    var user = String()
    
    let datasWithSession = ["Mis Eventos","Aviso de Privacidad","¿Necesitas ayuda?","Buzón de sugerencias","Acerca de","Cerrar sesión"]
    let datasWithoutSession = ["Aviso de Privacidad","¿Necesitas ayuda?","Buzón de sugerencias","Acerca de"]
    
    let titleText: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "AvenirNext-Regular", size: 14)
        lbl.textColor = UIColor.white
        lbl.textAlignment = .center
        lbl.text = "Únete a MasBoletos"
        //lbl.backgroundColor = UIColor.white
        return lbl
    }()
    
    let subTitleText: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "AvenirNext-Regular", size: 14)
        lbl.textColor = UIColor.white
        lbl.text = "Y comienza a gozar de todos los beneficios que tenemos para ti"
        lbl.textAlignment = .center
        lbl.numberOfLines = 3
        //lbl.backgroundColor = UIColor.white
        return lbl
    }()
    
    let btn_tengoCuenta: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Ya tengo cuenta", for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.backgroundColor = UIColor.clear
        btn.titleLabel?.font = UIFont(name: "AvenirNext-Regular", size: 15)
        btn.addTarget(self, action: #selector(handleLogin(_:)), for: .touchUpInside)
        btn.tag = 1
        btn.layer.cornerRadius = 17.5
        return btn
    }()
    
    let btn_IniciarSesion: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Crear un perfil ahora", for: .normal)
        btn.titleLabel?.font = UIFont(name: "AvenirNext-Regular", size: 15)
        btn.setTitleColor(UIColor.colorWithHexString(hexStr: "050349"), for: .normal)
        btn.backgroundColor = UIColor.white
        btn.addTarget(self, action: #selector(handleLogin(_:)), for: .touchUpInside)
        btn.tag = 2
        btn.layer.cornerRadius = 20
        return btn
    }()
    
    let imageViewProfile: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "if_profle")
        img.translatesAutoresizingMaskIntoConstraints = false
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewProfile.delegate = self
        tableViewProfile.dataSource = self
        tableViewProfile.separatorStyle = .none
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if let index = tableViewProfile.indexPathForSelectedRow {
            self.tableViewProfile.deselectRow(at: index, animated: true)
        }
        tableViewProfile.reloadData()
        
        view.reloadInputViews()
        setupVistas()
    }
    
    private func setupVistas() {
    
        if !UserDefaults.standard.isLoggedIn() {
            [titleText,subTitleText,btn_tengoCuenta,btn_IniciarSesion].forEach {viewProfileChange.addSubview($0) }
            
            titleText.text = "Únete a MasBoletos"
            titleText.textAlignment = .center
            titleText.anchor(top: viewProfileChange.topAnchor, leading: viewProfileChange.leadingAnchor, bottom: nil, trailing: viewProfileChange.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 28))
            
            subTitleText.anchor(top: titleText.bottomAnchor, leading: viewProfileChange.leadingAnchor, bottom: nil, trailing: viewProfileChange.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 50))
            
            btn_IniciarSesion.anchor(top: subTitleText.bottomAnchor, leading: subTitleText.leadingAnchor, bottom: nil, trailing: subTitleText.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 40))
            
            btn_tengoCuenta.anchor(top: btn_IniciarSesion.bottomAnchor, leading: subTitleText.leadingAnchor, bottom: nil, trailing: subTitleText.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 35))
            
            subTitleText.isHidden = false
        }else{
            
            [imageViewProfile, titleText].forEach {viewProfileChange.addSubview($0)}
            viewProfileChange.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 100))
            
            imageViewProfile.anchor(top: viewProfileChange.topAnchor, leading: viewProfileChange.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 8, left: 16, bottom: 0, right: 0), size: .init(width: 80, height: 70))
            
            titleText.anchor(top: viewProfileChange.topAnchor, leading: imageViewProfile.trailingAnchor, bottom: nil, trailing: viewProfileChange.trailingAnchor, padding: .init(top: 25, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 30))
            
            titleText.text = "Bienvenido: \(UserDefaults.standard.getUserName())"
            titleText.textAlignment = .center
            subTitleText.isHidden = true
        }
    }
    
    @objc func handleLogin(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            self.performSegue(withIdentifier: "segueToLVC", sender: self)
            break
        case 2:
            if let requestUrl = URL(string: "https://www.masboletos.mx/crearperfil.php") {
                UIApplication.shared.open(requestUrl as URL, options: [:], completionHandler: nil)
            }
            break
        default:
            break
        }
    }
        
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UserDefaults.standard.isLoggedIn() {
            return datasWithSession.count
        }else{
            return datasWithoutSession.count
        }
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellProfileTVC", for: indexPath) as UITableViewCell
        
        if (UserDefaults.standard.isLoggedIn()) {
            cell.textLabel?.text = "\(datasWithSession[indexPath.row])"
            cell.tag = indexPath.row
        }else{
            print(datasWithoutSession.count)
            print(indexPath.row)
            cell.textLabel?.text = "\(datasWithoutSession[indexPath.row])"
            cell.tag = indexPath.row
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UserDefaults.standard.isLoggedIn() {
            switch indexPath.row {
            case 0:
                
                self.performSegue(withIdentifier: "segueToMisboletos", sender: self)
                
            case 2:
                makeCall(mobileNumber: "4422122496")
            case 3:
                self.performSegue(withIdentifier: "segueToCuestionario", sender: self)
            case 4:
                self.performSegue(withIdentifier: "segueToCredits", sender: self)
            case 1:
                if let requestUrl = URL(string: "https://www.masboletos.mx/politicascompra.php") {
                    UIApplication.shared.open(requestUrl as URL, options: [:], completionHandler: nil)
                }
            case 5:
                UserDefaults.standard.removeUser()
                reloadViewFromNib()
                setupVistas()
                tableView.reloadData()
            default:
                print(tableView.tag)
            }
        }else{
            switch indexPath.row {
            case 0:
                if let requestUrl = URL(string: "https://www.masboletos.mx/politicascompra.php") {
                    UIApplication.shared.open(requestUrl as URL, options: [:], completionHandler: nil)
                }
            case 1:
                makeCall(mobileNumber: "4422122496")
            case 2:
                self.performSegue(withIdentifier: "segueToCuestionario", sender: self)
            case 3:
                self.performSegue(withIdentifier: "segueToCredits", sender: self)
            default:
                print(tableView.tag)
            }
        }
    }
    
    private func makeCall(mobileNumber:String) {
        
        if let mobileCallUrl = URL(string: "tel://\(mobileNumber)") {
            let application = UIApplication.shared
            if application.canOpenURL(mobileCallUrl) {
                if #available(iOS 10, *) {
                    application.open(mobileCallUrl)
                } else {
                    application.openURL(mobileCallUrl)
                }
            }
        }
    }
 

}

extension UIViewController {
    func reloadViewFromNib() {
        let parent = view.superview
        view.removeFromSuperview()
        view = nil
        parent?.addSubview(view) // This line causes the view to be reloaded
    }
}
