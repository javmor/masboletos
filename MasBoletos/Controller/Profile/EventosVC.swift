//
//  EventosVC.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/4/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EventosVC: UIViewController {
    
    var data :[[String]] = [
        ["Transacción","Evento","Fecha","Estatus","Boleto Electrónico"]
    ]
    
    //TRansaccion Evento Fecha Estatus
    
    @IBOutlet weak var cllEventos: TabularCollectionView!
    var isEventsOrPastEvents: Int!
    
    var array_eventos = [UserEventsModel]()
    var array_past_eventos = [PastUserEventsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCollectionView()
        setup()
    }

    
    private func initCollectionView() {
        cllEventos.tabularDelegate = self
        cllEventos.tabularDatasource = self
    }
    
    private func setup() {
        switch isEventsOrPastEvents {
        case 0:
            title = "Mis Eventos"
            downloadUserEvents()
        case 1:
            title = "Mis Eventos Pasados"
            data.removeAll()
            data.append(["Transacción","Evento","Fecha","Estatus"])
            downloadPastUserEventos()
        default:
            break
        }
    }
    
    private func downloadUserEvents() {
        let urlString = "https://www.masboletos.mx/appMasboletos/getMisEventosUsuario.php?idcliente=\(UserDefaults.standard.getUserID())"
        print(urlString)
        Alamofire.request(urlString).responseJSON { response in
            switch response.result {
            case .success:
                do{
                    let jsonData = try JSON(data: response.data!)
                    let array = jsonData.arrayValue
                    dump(array)
                    for element in array {
                        let imagen = element["element"].stringValue
                        let cantidad = element["cantidad"].intValue
                        let evento = element["evento"].stringValue
                        let fechaevento = element["fechaevento"].stringValue
                        let estatus = element["estatus"].stringValue
                        let idforma = element["idforma"].intValue
                        let transaccion = element["transaccion"].stringValue
                        
                        self.array_eventos.append(UserEventsModel(imagen: imagen, cantidad: cantidad, evento: evento, fecha_evento: fechaevento, estatus: estatus, idForma: idforma, transaccion: transaccion))
                        
                        self.data.append([transaccion,evento,fechaevento,estatus, String(cantidad)])
                    }
                    
                    self.cllEventos.reloadData()
                    
                }catch{
                    print("Error")
                }
                break
            case .failure(let error):
                print("Error \(error)")
            }
        }
    }
    
    private func downloadPastUserEventos() {
        
        let urlString = "https://www.masboletos.mx/appMasboletos/getMisEventosPasadosUsuario.php?idcliente=\(UserDefaults.standard.getUserID())"
        
        Alamofire.request(urlString).responseJSON { response in
        
            switch response.result {
            case .success:
                do{
                    let jsondata = try JSON(data: response.data!)
                    let datas = jsondata.arrayValue
                    
                    for element in datas {
                        let imagen = element["element"].stringValue
                        let cantidad = element["cantidad"].intValue
                        let evento = element["evento"].stringValue
                        let fechaevento = element["fechaevento"].stringValue
                        let estatus = element["estatus"].stringValue
                        let transaccion = element["transaccion"].stringValue
                    
                        self.data.append([transaccion,evento,fechaevento,estatus])
                        self.array_past_eventos.append(PastUserEventsModel(imagen: imagen, cantidad: cantidad, evento: evento, fecha_evento: fechaevento, estatus: estatus, transaccion: transaccion))
                        
                    }
                    self.cllEventos.reloadData()
                }catch {
                    print("Error JSON")
                }
                break
            case .failure(let error):
                print("Error \(error)")
                break
            }
        }
    }
}

extension EventosVC: TabularCollectionDataSource, TabularCollectionDelegate {
    
    func tabularView(_ tabularView: TabularCollectionView, titleAttributesForCellAt indexpath: IndexPath) -> CellTitleAttributes {
        var font = Font.avenirMedium.font()
        var textAlignment = NSTextAlignment.center
        if indexpath.section == 0 {
            font = Font.avenirHeavy.font(ofSize: 18)
        }
        if indexpath.row < 2 && indexpath.section != 0 {
            textAlignment = .center
        }
        let text = data[indexpath.section][indexpath.row]
        return CellTitleAttributes(title: text, font: font, textAlignment: textAlignment, textColor: Color.text.uiColor)
    }
    
    func numberOfColumns(in tabularView: TabularCollectionView) -> Int { return data.first?.count ?? 0 }
    
    func numberOfRows(in tabularView: TabularCollectionView) -> Int { return data.count }
    
    func numberOfStaticRows(in tabularView: TabularCollectionView) -> Int { return 1 }
    
    func numberOfStaticColumn(in tabularView: TabularCollectionView) -> Int { return 0 }
    
    @objc(tabularView:didSelectItemAt:) func tabularView(_ tabularView: TabularCollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.section != 0 {
            
        }
    }
    
    func tabularView(_ tabularView: TabularCollectionView, shouldHideColumnSeparatorAt indexPath: IndexPath) -> Bool {
        return indexPath.row != 0
    }
}
