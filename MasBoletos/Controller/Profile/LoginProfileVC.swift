    //
    //  LoginProfileVC.swift
    //  MasBoletos
    //
    //  Created by Victor Javier Arroyo Morales on 10/11/18.
    //  Copyright © 2018 IT-STAM. All rights reserved.
    //

    import UIKit
    import SwiftyJSON
    import SkyFloatingLabelTextField
    import Alamofire
    import AZDialogView

class LoginProfileVC: UIViewController {
        
    let imageView: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "avatar_hombre")
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    let tvUserName: SkyFloatingLabelTextField = {
        let txt = SkyFloatingLabelTextField(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        txt.placeholder = "Usuario"
        txt.title = "Usuario"
        txt.font = UIFont(name: "Avenir Next", size: 16)
        txt.lineHeight = 1.0 // bottom line height in points
        txt.selectedLineHeight = 2.0
        txt.selectedTitleColor = UIColor.black
        txt.autocapitalizationType = .none
        //txt.autocapitalizationType = .allCharacters
        txt.addDoneButtonOnKeyboard()
        return txt
    }()
    
    let tvUserPassword: SkyFloatingLabelTextField = {
        let txt = SkyFloatingLabelTextField(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        txt.placeholder = "Contraseña"
        txt.title = "Contraseña"
        txt.font = UIFont(name: "Avenir Next", size: 16)
        txt.lineHeight = 1.0 // bottom line height in points
        txt.selectedLineHeight = 2.0
        txt.isSecureTextEntry = true
        txt.selectedTitleColor = UIColor.black
        txt.addDoneButtonOnKeyboard()
        return txt
    }()
    
    let btn_sesion: UIButton = {
    
        let button = UIButton(
            style: .filled(cornerRadius: 27.5),
             theme: .extraLight)
        
        button.setTitle("Iniciar sesión", for: .normal)
        button.titleLabel?.font = UIFont(name: "AvenirNext", size: 14)
        button.addBorderToButton(cRadius: 27.5, bWidth: 1)
        button.addTarget(self,
                         action: #selector(checkBeforeSend),
                         for: .touchUpInside
        )
        return button
    }()
    
    var isKeyboardAppear = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        self.hideKeyboardWhenTappedAround()
        
        //NotificationCenter.default.addObserver(self, selector: #selector(LoginProfileVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(LoginProfileVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    private func setup() {
        
        [imageView, tvUserName, tvUserPassword, btn_sesion].forEach{ view.addSubview($0) }
        
        if #available(iOS 11.0, *) {
            imageView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 12, left: 48, bottom: 0, right: 48))
        } else {
            imageView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 12, left: 48, bottom: 0, right: 48))
        }
        
        tvUserName.anchor(top: imageView.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 16, left: 16, bottom: 0, right: 16), size: .init(width: 0, height: 45))
        
        tvUserPassword.anchor(top: tvUserName.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 16, left: 16, bottom: 0, right: 16), size: .init(width: 0, height: 45))
        
        btn_sesion.anchor(top: tvUserPassword.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 32, left: 16, bottom: 0, right: 16), size: .init(width: 0, height: 55))
        
    }

    @objc func checkBeforeSend() {
        let _email = tvUserName.text!
        let _password = tvUserPassword.text!
        
        
        guard tvUserName.text?.count != 0  else {
            alerts(title: "", menssage: "Por favor ingresa tu email")
            return
        }
        
        if isValidEmail(emailID: _email) == false {
            alerts(title: "", menssage: "Ingresa una dirección de correo válida")
            
        }
        
        guard tvUserPassword.text?.count != 0  else {
            alerts(title: "", menssage: "Por favor ingresa tu contraseña")
            return
        }
        
        let params: [String : String] = [
            "correo": _email,
            "contrasenia": _password,
            "tipo": "1"
        ]
        checkUser(params: params)
    }

    func isValidEmail(emailID:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailID)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if !isKeyboardAppear {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                if self.view.frame.origin.y == 0{
                    self.view.frame.origin.y -= keyboardSize.height
                }
            }
            isKeyboardAppear = true
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if isKeyboardAppear {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                if self.view.frame.origin.y != 0{
                    self.view.frame.origin.y += keyboardSize.height
                }
            }
            isKeyboardAppear = false
        }
    }

    private func checkUser(params: [String:String]) {
        
        let urlString = "https://www.masboletos.mx/appMasboletos/validalogin.php"
        let headers = [ "Content-Type" : "application/x-www-form-urlencoded"]
        
        Alamofire.request(urlString, method: .post, parameters: params,encoding: URLEncoding.httpBody, headers: headers ).responseJSON {
            response in
            
            switch response.result {
                
            case .success:
                do {
                    let jsonArray = try JSON(data: response.data!)
                    let json = jsonArray.arrayValue
                    
                    
                    for element in json {
                        let id_user = element["id_cliente"].intValue
                        let name_usuario = element["usuario"].stringValue
                        let mensaje = element["mensaje"].stringValue
                        let respuesta = element["respuesta"].boolValue
                        
                        if (respuesta) {
                            print("\(id_user) :  \(name_usuario) : \(mensaje)")
                            UserDefaults.standard.setUserID(value: id_user)
                            UserDefaults.standard.setUserName(value: name_usuario)
                            UserDefaults.standard.setLoggedIn(value: true)
                            
                            let dialog = AZDialogViewController(title: "Bienvenido de nuevo: ", message: "\(id_user)")
                            dialog.addAction(AZDialogAction(title: "Regresar a tu perfil") { (dialog) -> (Void) in
                                self.navigationController?.popToRootViewController(animated: true)
                                dialog.dismiss()
                            })
                            self.present(dialog, animated: false, completion: nil)
                        }else{
                            let dialog = AZDialogViewController(title: "Error", message: "\(mensaje)")
                            dialog.addAction(AZDialogAction(title: "Ok") { (dialog) -> (Void) in
                                dialog.dismiss()
                            })
                        }
                    }
                    
                }catch{
                    print("Error JSON")
                }
                
            case .failure(let error):
                print("Error: \(error)")
                
            }
        }
    }

    private func alerts(title: String, menssage: String) {
        let alert = UIAlertController(title: "\(title)", message: "\(menssage)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
}
