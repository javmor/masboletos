//
//  ViewController.swift
//  MasBoletos
//
//  Created by Javier Morales on 7/23/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import NVActivityIndicatorView
import SwiftyJSON
import Parchment

import ViewAnimator

class MainVC: UIViewController {
    
    var array_patrocinadores = [Patrocinadores]()
    var array_paquetes = [PaquetesOrganizadoresModel]()
    
    private var selectedNum = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let eventosIndividualesVC = storyboard.instantiateViewController(withIdentifier: "EventosIndividualesVC")
        let paquetesVC = storyboard.instantiateViewController(withIdentifier: "VentaPaquetesVC")
        
        let pagingViewController = FixedPagingViewController(viewControllers: [
            eventosIndividualesVC, paquetesVC])
        
        addChild(pagingViewController)
        view.addSubview(pagingViewController.view)
        pagingViewController.view.fillSuperview()
        pagingViewController.didMove(toParent: self)
        pagingViewController.selectedBackgroundColor = UIColor.colorWithHexString(hexStr: "050349")
        pagingViewController.backgroundColor = UIColor.colorWithHexString(hexStr: "050349")
        pagingViewController.selectedTextColor = UIColor.colorWithHexString(hexStr: "99BF10")
        pagingViewController.textColor = UIColor.white
        pagingViewController.menuBackgroundColor = UIColor.colorWithHexString(hexStr: "050349")
        pagingViewController.indicatorColor = UIColor.colorWithHexString(hexStr: "99BF10")
        addNavBarImage()
        
        view.backgroundColor = UIColor.colorWithHexString(hexStr: "F6F6F6")
    }
    private func getPatrocinadores() {
        
        let urlString = "https://www.masboletos.mx/appMasboletos/getPatrocinadores.php"
        
        Alamofire.request(urlString).responseJSON { response in
            
            switch response.result{
            case .success:
                do{
                    let jsonData = try JSON(data: response.data!)
                    let arr = jsonData.arrayValue
                    
                    for item in arr {
                        let idorganizador = item["idorganizador"].intValue
                        let nombre = item["nombre"].stringValue
                        let banner = item["banner"].stringValue
                        let url = "https://www.masboletos.mx/sica/imgEventos/\(banner)"
                        self.array_patrocinadores.append( Patrocinadores(id_organizador: idorganizador, nombre: nombre, banner: url))
                    }
                    
                }catch{
                    print("Error")
                }
                break
            case .failure(let error):
                print("Error: \(error)")
            }
        }
    }
    
    private func addNavBarImage() {
        
        let navController = navigationController!
        navController.navigationBar.shadowImage = UIImage()
        
        let image = UIImage(named: "logo_masboletos.png") //Your logo url here
        let imageView = UIImageView(image: image)
        
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height
        
        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2
        
        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth, height: 50)
        imageView.contentMode = .scaleAspectFit
        
        navigationItem.titleView = imageView
    }
    
    private func checkInternet() {
        if Reachability.isConnectedToNetwork() {
            print("Internet connection OK")
        } else {
            alert(Message: "Dispositivo sin conexión a internet. Por favor verifique su conexión a internet.")
        }
    }
    
    private func alert(Message: String) {
        let alert = UIAlertController(title: "SIN CONEXIÓN A INTERNET", message: Message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

