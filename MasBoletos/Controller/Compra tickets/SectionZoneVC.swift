//
//  SecondViewController.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 8/5/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit
import Alamofire
import McPicker
import SwiftyJSON
import SDWebImage

class SectionZoneVC: UIViewController {

    let evento_grupo = [Int]()
    let imageNombre = [String]()
    
    var arraySeccionPicker = [String]()
    var arrayZonaPicker = [String]()
    
    @IBOutlet weak var btnMejorDisponible: UIButton!
    @IBOutlet weak var btnSeccion: UIButton!
    @IBOutlet weak var imageZonaEvento: UIImageView!
    @IBOutlet weak var pickerViewSegundaSeccion: UIStackView!
    @IBOutlet weak var switchAsientos: UISwitch!
    @IBOutlet weak var labelZonas: UILabel!
    @IBOutlet weak var labelSeccion: UILabel!
    
    var checkSwitchAsientos = Int()
    var numerado: Int!
    var numeradoSeccion: Int!
    var getSeccionByZona: String!
    var seccion = String()
    var precio = Int()
    var countTickets = Int()
    var valueEventoFuncion: Int!
    var idZona = Int()
    var nombre_evento = String()
    var fila_seleccion = String()
    var fecha_evento = String()
    var hora_evento = String()
    var direccion_evento: String?
    var more_information: String?
    var url_header_image: String?
    var index_numerado: Int!
    var lugar: String?
    
    let nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.addSettingsButtonBarNextEnabled()
        return button
    }()
    
    var arrayZonasPickerModel = [ZonasPickerModel]()
    var array_mejorDisponible = [ModelMejorDisponible]()
    var array_seccion = [ModelSeccion]()
    
    
    //MARK: init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        downloadZonaPicker(evento_id: valueEventoFuncion!)
        title = "Asientos"
        setupButtonNextNavBarAndButtons()
        view.backgroundColor = UIColor.colorWithHexString(hexStr: "F6F6F6")
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        switchAsientos.setOn(false, animated: true)
        checkSwitchAsientos = 0
        nextButton.setTitle("SIGUIENTE", for: .normal)
    }
    
    //MARK: Selectors
    @objc private func addTapped() {
        
        if !array_mejorDisponible.isEmpty {
            if array_mejorDisponible[0].mensagesetTipo == 1 {
                if checkSwitchAsientos == 0 {
                    self.performSegue(withIdentifier: "segueToWithoutBooking", sender: self)
                } else {
                    self.performSegue(withIdentifier: "segueToSeatBooking", sender: self)
                }
            } else {
                alerts(title: "BOLETOS NO DISPONIBLES", menssage: "\(array_mejorDisponible[0].mensagesetMensage) Solicite una cantidad diferente o verifique la zona")
            }
        }else{
            alerts(title: "", menssage: "Cantidad de boletos NO disponible")
        }

    }

    @IBAction func btnZonas(_ sender:UIButton!) {
        
        McPicker.show(data: [arrayZonaPicker]) {  [weak self] (selections: [Int : String], rowPosition: Int) -> Void in
            
            if let name = selections[0] {
                self?.arraySeccionPicker.removeAll()
                self?.getSeccionByZona = self!.arrayZonasPickerModel[rowPosition].grupo //ARRAY GRUPO
                self!.precio = self!.arrayZonasPickerModel[rowPosition].precio

                self?.downloadSeccionPicker(evento_id: ((self?.valueEventoFuncion)!), grupo: (self?.getSeccionByZona)!)

                self!.seccion = self!.arrayZonasPickerModel[rowPosition].grupo
                
                self?.labelZonas.text = name
                self?.pickerViewSegundaSeccion.isHidden = true
                self?.btnSeccion.isEnabled = true
                self?.btnSeccion.alpha = 1.0
                self?.labelSeccion.text = ""
                self?.index_numerado = self?.arrayZonasPickerModel[rowPosition].numerado
            }
        }
    }
    
    
    @IBAction func btnSeccion(_ sender: Any) {
        
        McPicker.show(data: [arraySeccionPicker]) {  [weak self] (selections: [Int : String], rowPosition: Int) in
            if let name = selections[0] {
                
                if self?.arraySeccionPicker[rowPosition] == "MEJOR SELECCIÓN" {
                    self?.labelSeccion.text = name
                    self?.pickerViewSegundaSeccion.isHidden = true
                    self?.nextButton.isEnabled = true
                    self?.nextButton.alpha = 1.0
                    self!.idZona = 0
                }
            
                if self?.array_seccion[rowPosition]._numerado == 1 {
                    self?.labelSeccion.text = name
                    self?.idZona = (self?.array_seccion[rowPosition]._idZona)!
                    self?.pickerViewSegundaSeccion.isHidden = false
                }
                self?.labelSeccion.text = name
                self?.nextButton.isEnabled = true
                self?.nextButton.alpha = 1.0
            }
        }
        
        self.betterPlace()
    }
    
    @objc func stateChanged(sender: UISwitch) {
        if switchAsientos.isOn {
            nextButton.setEnabledButtonBarNext()
            nextButton.setTitle("MEJOR DISPONIBLE", for: .normal)
            checkSwitchAsientos = 1
        } else {
            nextButton.setTitle("SIGUIENTE", for: .normal)
            nextButton.setEnabledButtonBarNext()
            checkSwitchAsientos = 0
        }
    }
    
    
    //MARK: Helpers
    
    private func setupButtonNextNavBarAndButtons() {
        let buttonNavBarItem = UIBarButtonItem.init(customView: nextButton)
        nextButton.isEnabled = false
        nextButton.alpha = 0.10
        nextButton.addTarget(self, action: #selector(addTapped), for: .touchUpInside)
        navigationItem.rightBarButtonItems = [buttonNavBarItem]
        
        btnMejorDisponible.layer.cornerRadius = 20
        btnMejorDisponible.addTarget(self, action: #selector(btnZonas(_:)), for: .touchUpInside)
        btnMejorDisponible.backgroundColor = UIColor.mainBlue()
        btnMejorDisponible.titleLabel?.textColor = .white
        
        btnSeccion.layer.cornerRadius = 20
        btnSeccion.addTarget(self, action: #selector(btnSeccion(_ :)), for: .touchUpInside)
        btnSeccion.backgroundColor = UIColor.mainBlue()
        btnSeccion.titleLabel?.textColor = .white
        btnSeccion.isEnabled = false
        
        switchAsientos.addTarget(self, action: #selector(stateChanged(sender:)), for: .valueChanged)
    }
    
    private func downloadZonaPicker(evento_id: Int) {
        
        showHUD(progressLabel: "Cargando")
        let urlString = "http://www.masboletos.mx/appMasboletos/getZonasxEvento.php?idevento=\(evento_id)"
        Alamofire.request(urlString).responseJSON { response in
            
            switch response.result {
            case .success:
                
                do{
                    let jsonDatas = try JSON(data: response.data!)
                    let json = jsonDatas.arrayValue
                    
                    if (jsonDatas.isEmpty) {
                        self.alerts(title: "EVENTO SIN FECHA", menssage: "Evento no disponible para su venta.")
                        self.btnMejorDisponible.isEnabled = false
                        self.btnMejorDisponible.alpha = 0.45
                        self.btnSeccion.isEnabled = false
                        self.btnSeccion.alpha = 0.45
                    }else{
                        for elements in json {
                            let precio = elements["precio"].intValue
                            let grupo = elements["grupo"].stringValue
                            let disponibilidad = elements["disponibilidad"].intValue
                            let name_image = elements["EventoMapam"].stringValue
                            
                            var urlImage = "http://www.masboletos.mx/sica/imgEventos/"+name_image
                            urlImage = urlImage.replacingOccurrences(of: " ", with: "%20")
                            self.imageZonaEvento.sd_setImage(with: URL(string: urlImage), placeholderImage: UIImage(named: "logo_masboletos"))
                            
                            let numerado = elements["numerado"].intValue
                            
                            let complete = grupo + " $\(precio).00 C/U Disponibilidad: \(disponibilidad)"
                            self.arrayZonasPickerModel.append(ZonasPickerModel(precio: precio, grupo: grupo, disponibilidad: disponibilidad, EventoMapam: urlImage, numerado: numerado))
                            self.arrayZonaPicker.append(complete)
                        }
                    }
                    
                    self.dismissHUD(isAnimated: true)
                    
                }catch{
                    print("Error")
                }
                
                break
            case .failure(let error):
                print("Error: \(error)")
            }
        }
    }

    private func downloadSeccionPicker(evento_id: Int, grupo: String) {
        
        let aux_grupo = grupo.replacingOccurrences(of: " ", with: "%20")
        
        arraySeccionPicker.removeAll()
        array_seccion.removeAll()
       
        showHUD(progressLabel: "Cargando")
        let urlString = "https://www.masboletos.mx/appMasboletos/getSubzonasxGrupo.php?idevento=\(evento_id)&grupo=\(aux_grupo)"
        print(urlString)
        Alamofire.request(urlString).responseJSON { response in
            
            switch response.result {
            case .success:
                do{
                    let json = try JSON(data: response.data!)
                    let datas = json.arrayValue
                    
                    self.arraySeccionPicker.append("MEJOR SELECCIÓN")
                    self.array_seccion.append(ModelSeccion(_idZona: 9999, _nombre: "MEJOR SELECCION", _numerado: 9999))
                    
                    for item in datas {
                        let idzona = item["idzona"].intValue
                        let nombre = item["nombre"].stringValue
                        let numeradoSeccion = item["numerado"].intValue
                        
                        self.arraySeccionPicker.append(nombre)
                        self.array_seccion.append(ModelSeccion(_idZona: idzona, _nombre: nombre, _numerado: numeradoSeccion))
                    }
                    self.dismissHUD(isAnimated: true)
                    
                }catch{
                    print("Error")
                }
            case .failure(let error):
                print("Error: \(error)")
            }
        }
        
    }
    
    
    private func betterPlace() {
        print(getSeccionByZona!)
        getSeccionByZona = getSeccionByZona.replacingOccurrences(of: " ", with: "%20")
        
        let urlString = "https://www.masboletos.mx/appMasboletos/getBoletos.php?idevento=\(valueEventoFuncion!)&numerado=\(index_numerado!)&zona=\(getSeccionByZona!)&CantBoletos=\(countTickets)&idzonaxgrupo=\(idZona)"
    
        print(urlString)
        Alamofire.request(urlString).responseJSON { response in
            
            switch response.result {
            case .success:
                do{
                    let json = try JSON(data: response.data!)
                    let datas = json.arrayValue
                    print(datas)
                    
                    for item in datas {
                        let _mensagesetTipo = item["mensagesetTipo"].intValue
                        var _mensagesetMensage = item["mensagesetMensage"].stringValue
                        
                        if _mensagesetTipo == 1 {
                            _mensagesetMensage = item["mensagesetMensage"].stringValue
                            let _mensagesetIdZona = item["mensagesetIdZona"].intValue
                            let _mensagesetFila = item["mensagesetFila"].stringValue
                            let _mensagesetIniColumna = item["mensagesetIniColumna"].intValue
                            let _mensagesetFinColumna = item["mensagesetFinColumna"].intValue
                            let _idfila = item["idfila"].intValue
                            let _mensagesetAsientos = item["mensagesetAsientos"].stringValue
                            self.fila_seleccion = ""
                            
                            self.array_mejorDisponible.append(ModelMejorDisponible(mensagesetTipo: _mensagesetTipo, mensagesetMensage: _mensagesetMensage, mensagesetIdZona: _mensagesetIdZona, mensagesetFila: _mensagesetFila, mensagesetIniColumna: _mensagesetIniColumna, mensagesetFinColumna: _mensagesetFinColumna, idfila: _idfila, mensagesetAsientos: _mensagesetAsientos, mensagesetDescripcion: ""))
                            
                        }else if _mensagesetMensage == "Aforo no disponible." {
                            print("Aforo no disponible")
                        }
                    }
                }catch{ print("Error SwiftyJSON") }
            
            case .failure(let error):
                print("Error \(error)")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueToSeatBooking" {
            if let thirdVC = segue.destination as? SeatSelectVC {
                thirdVC.checkSwitchAsientos = checkSwitchAsientos
                thirdVC.precio = precio
                thirdVC.seccion = seccion
                thirdVC.countTickets = countTickets
                thirdVC.valueEventoFuncion = valueEventoFuncion
                thirdVC.idZona = idZona
                thirdVC.fecha_evento = fecha_evento
                thirdVC.hora_evento = hora_evento
                thirdVC.nombre_evento = nombre_evento
                thirdVC.index_numerado = index_numerado
            }
        }
        
        if segue.identifier == "segueToWithoutBooking" {
            if let showPayments = segue.destination as? SummaryPaymenthVC {
                showPayments.countTickets = countTickets
                showPayments.precio = precio
                showPayments.seccion = seccion
                showPayments.valueEventoFuncion = valueEventoFuncion
                
                showPayments.idZona = array_mejorDisponible[0].mensagesetIdZona
                showPayments.idfila = array_mejorDisponible[0].idfila
                showPayments.mensagesetFila = array_mejorDisponible[0].mensagesetFila
                showPayments.mensagesetIniColumna = array_mejorDisponible[0].mensagesetIniColumna
                showPayments.mensagesetFinColumna = array_mejorDisponible[0].mensagesetFinColumna
                showPayments.mensagesetAsientos = array_mejorDisponible[0].mensagesetAsientos
                
                showPayments.fecha_evento = fecha_evento
                showPayments.hora_evento = hora_evento
                showPayments.nombre_evento = nombre_evento
                showPayments.index_numerado = index_numerado
                showPayments.direccion_evento = direccion_evento
                showPayments.url_header_image = url_header_image
                showPayments.more_information = more_information
            }
        }
        
    }
    
    private func alerts(title: String, menssage: String) {
        let alert = UIAlertController(title: "\(title)", message: "\(menssage)", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
        self.present(alert, animated: true)
    }

}

