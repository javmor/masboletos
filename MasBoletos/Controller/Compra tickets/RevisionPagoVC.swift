//
//  RevisionPagoViewController.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 9/20/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class RevisionPagoVC: UIViewController, PayPalPaymentDelegate {

    var jsonArray: NSArray?
    
    var idZona = Int()
    var countTickets = Int()
    var valueEventoFuncion = Int()
    var seccion = String()
    var precio = Int()
    
    var fecha_evento = String()
    var hora_evento = String()
    var nombre_evento = String()
    var total_asientos = String()
    var total_fila_asiento_id = String()
    var metodo_de_pago = String()
    var metodo_de_entrega = String()
    
    var costo_metodo_de_pago = Double()
    var importe_fijo_metodo_pago = Int()
    var idfila = Int()
    var costo_metodo_de_entrega = Double()
    
    var idMetodoPago = Int()
    var idFormaEntrega = Int()
    
    var index_numerado = Int()
    var id_transaccion = Int()
    var id_fe = Int()
    var mensagesetIdZona: Any = ""
    var mensagesetFila = String()
    var mensagesetIniColumna = Int()
    var mensagesetFinColumna = Int()
    
    var totalTodo = Double()
    var direccion_evento: String?
    var more_information: String?
    var url_header_image: String?
    
    @IBOutlet weak var eventolbl: UILabel!
    @IBOutlet weak var fechalbl: UILabel!
    @IBOutlet weak var horalbl: UILabel!
    
    @IBOutlet weak var boletosInfoLabel: UILabel!
    @IBOutlet weak var boletoInfoPreciolbl: UILabel!
    
    @IBOutlet weak var total_asientoslbl: UILabel!

    @IBOutlet weak var forma_entregalbl: UILabel!
    @IBOutlet weak var cargo_forma_entregalbl: UILabel!
    
    @IBOutlet weak var formaPagolbl: UILabel!
    @IBOutlet weak var cargoFormadePagolbl: UILabel!
    
    @IBOutlet weak var cobroTotallbl: UILabel!
    
    @IBOutlet weak var switchDeAcuerdo: UISwitch!
    
    let nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("SIGUIENTE", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 4
        button.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        button.layer.borderColor = UIColor.white.cgColor
        return button
    }()
    
    var payPalConfig = PayPalConfiguration()
    
    var environment:String = PayPalEnvironmentSandbox {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Confirmar Compra"
        paypalConfigure()
        setupButtonNextNavBar()
        setupLabelsInformation()
        switchDeAcuerdo.addTarget(self, action: #selector(switchChangeValue(sender:)), for: .valueChanged)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserDefaults.standard.isLoggedIn() {
            nextButton.setTitle("PAGAR", for: .normal)
        }
        
        
    }
    
    
    @objc func switchChangeValue(sender: UISwitch) {
        
        if switchDeAcuerdo.isOn {
            nextButton.alpha = 1.0
            nextButton.isEnabled = true
        }else{
            nextButton.alpha = 0.10
            nextButton.isEnabled = false
        }
    }
    
    private func setupButtonNextNavBar() {
        let buttonNavBarItem = UIBarButtonItem.init(customView: nextButton)
        nextButton.isEnabled = false
        nextButton.alpha = 0.10
        nextButton.addTarget(self, action: #selector(addTapped), for: .touchUpInside)
        navigationItem.rightBarButtonItems = [buttonNavBarItem]
    }
    
    @objc private func addTapped() {
        
        if UserDefaults.standard.isLoggedIn() {
            if idMetodoPago == 5 {
                setPayPayPal()
            }
            if idMetodoPago == 2 || idMetodoPago == 3 {
                self.performSegue(withIdentifier: "segueToWebView", sender: self)
            }
        }else{
            self.performSegue(withIdentifier: "segueSignUp", sender: self)
        }
        
    }
    
    private func alerts(title: String, menssage: String) {
        let alert = UIAlertController(title: "\(title)", message: "\(menssage)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    private func setupLabelsInformation() {
        eventolbl.text = nombre_evento.replacingOccurrences(of: "%20", with: " ")
        fechalbl.text = fecha_evento
        horalbl.text = hora_evento
        
        boletosInfoLabel.text = "\(seccion) x \(countTickets)"
        boletoInfoPreciolbl.text = "$\(countTickets * precio).00"
        
        total_asientoslbl.text = total_asientos
        formaPagolbl.text = metodo_de_pago
        forma_entregalbl.text = metodo_de_entrega
        
        let aux_metodo_pago = Double(importe_fijo_metodo_pago) + costo_metodo_de_pago
        cargoFormadePagolbl.text = "$\(Double(importe_fijo_metodo_pago) + costo_metodo_de_pago)0"
        let aux_costo_entrega = costo_metodo_de_entrega * Double(countTickets)
        cargo_forma_entregalbl.text = "$\(String(format: "%.2f", Double(aux_costo_entrega)))"
        
        let totalTickets = countTickets * precio
        totalTodo = Double(totalTickets) + Double(aux_costo_entrega) + aux_metodo_pago
        
        cobroTotallbl.text = "$\(totalTodo)0"
        
    }
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            self.setPaymentPaypal(error: 0)
            self.performSegue(withIdentifier: "segueNextToPayPal", sender: self)
            
        })
        
    }
    
    private func paypalConfigure() {
        payPalConfig.acceptCreditCards = false;
        payPalConfig.merchantName = "+Boletos"
        payPalConfig.merchantPrivacyPolicyURL = NSURL(string: "https://www.sivaganesh.com/privacy.html")! as URL
        payPalConfig.merchantUserAgreementURL = NSURL(string: "https://www.sivaganesh.com/useragreement.html")! as URL
        //payPalConfig.languageOrLocale = NSLocale.preferredLanguages[0]
        payPalConfig.languageOrLocale = "es_MX"
        payPalConfig.payPalShippingAddressOption = .payPal;
        PayPalMobile.preconnect(withEnvironment: environment)
    }
    
    private func setPayPayPal() {
        
        pulledApartTicketsPayPal()
        
        let amount = NSDecimalNumber(string: "\(totalTodo)")
        let payment = PayPalPayment()
        payment.amount = amount
        payment.currencyCode = "MXN"
        payment.shortDescription = "\(nombre_evento.replacingOccurrences(of: "%20", with: " "))"
        payment.intent = .sale
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self as PayPalPaymentDelegate)
            present(paymentViewController!, animated: true, completion: nil)
            
        } else {
            setPaymentPaypal(error: 1)
            print("Pago no procesado.\(payment)")
        }
        
    }
    
    fileprivate func pulledApartTicketsPayPal() {
        
        mensagesetFila = mensagesetFila.replacingOccurrences(of: " ", with: "")
        var aux_string = ""
        
        if idfila == 0 {
            aux_string = ""
        }else{
            aux_string = String(idfila)
            total_asientos = ""
        }
        
        let id_usuario = UserDefaults.standard.getUserID()
        let urlString = "https://www.masboletos.mx/masBoletosEnviaDatosPaypalMovil.php?idevento=\(valueEventoFuncion)&numerado=\(index_numerado)&cantidad=\(countTickets)&cargoxservicio=\(costo_metodo_de_pago+costo_metodo_de_entrega)&zona=\(idZona)&idcliente=\(id_usuario)&formadepago=\(idMetodoPago)&txtformaentrega=\(idFormaEntrega)&importe=\(precio)&idfila=\(aux_string)&inicolumna=\(mensagesetIniColumna)&fincolumna=\(mensagesetFinColumna)&fila=\(mensagesetFila)&idfilafilaasiento=\(total_fila_asiento_id)&filaasientos=\(total_asientos)"
        
        Alamofire.request(urlString,method: .get) .responseJSON { response in
            
            if let transaction = response.result.value {
                self.id_transaccion = transaction as! Int
                print(self.id_transaccion)
            }
        }
    }

    fileprivate func setPaymentPaypal (error: Int) {
        let urlString = "https://www.masboletos.mx/masMoletosRecibeDatosPaypalMovil.php?EM_OrderID=\(id_transaccion)&error=\(error)"
        
        Alamofire.request(urlString, method: .get).responseJSON { response in
            
            if let transaction = response.result.value {
                self.id_fe = transaction as! Int
            }
            self.sendEmail()
        }
    }
    
    private func sendEmail() {
        
        let urlString = "https://www.masboletos.mx/sica/masmail.cfm?trans=\(id_transaccion)&fe=\(id_fe)&de=2"
        print(urlString)
        
        Alamofire.request(urlString, method: .get).responseJSON { response in
            if let transaction = response.result.value {
               print(transaction)
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let lgVC = segue.destination as? LogInVC {
            
            lgVC.idZona = idZona
            lgVC.idMetodoPago = idMetodoPago
            lgVC.idFormaEntrega = idFormaEntrega
            lgVC.costo_metodo_de_pago = costo_metodo_de_pago
            lgVC.totalTodo = totalTodo
            lgVC.nombre_evento = nombre_evento
            lgVC.valueEventoFuncion = valueEventoFuncion
            lgVC.nombre_evento = nombre_evento
            lgVC.hora_evento = hora_evento
            lgVC.fecha_evento = fecha_evento
            lgVC.seccion = seccion
            lgVC.precio = precio
            lgVC.countTickets = countTickets
            lgVC.total_asientos = total_asientos
            lgVC.metodo_de_pago = metodo_de_pago
            lgVC.metodo_de_entrega = metodo_de_entrega
            lgVC.importe_fijo_metodo_pago = importe_fijo_metodo_pago
            lgVC.costo_metodo_de_pago = costo_metodo_de_pago
            lgVC.costo_metodo_de_entrega = costo_metodo_de_entrega
            lgVC.idMetodoPago = idMetodoPago
            lgVC.total_fila_asiento_id = total_fila_asiento_id
            lgVC.idFormaEntrega = idFormaEntrega
            lgVC.index_numerado = index_numerado
            lgVC.mensagesetFila = mensagesetFila
            lgVC.mensagesetIniColumna = mensagesetIniColumna
            lgVC.mensagesetFinColumna = mensagesetFinColumna
            lgVC.idfila = idfila
            lgVC.idZona = idZona
        }
        
        if let infoVC = segue.destination as? InformationPayVC {
            infoVC.id_transaccion = id_transaccion
        }
        
        if let webViewVC = segue.destination as? WebiViewPayVC {
            webViewVC.idZona = idZona
            webViewVC.idMetodoPago = idMetodoPago
            webViewVC.idFormaEntrega = idFormaEntrega
            webViewVC.costo_metodo_de_pago = costo_metodo_de_pago
            webViewVC.totalTodo = totalTodo
            webViewVC.nombre_evento = nombre_evento
            webViewVC.valueEventoFuncion = valueEventoFuncion
            webViewVC.nombre_evento = nombre_evento
            webViewVC.hora_evento = hora_evento
            webViewVC.fecha_evento = fecha_evento
            webViewVC.seccion = seccion
            webViewVC.precio = precio
            webViewVC.countTickets = countTickets
            webViewVC.total_asientos = total_asientos
            webViewVC.metodo_de_pago = metodo_de_pago
            webViewVC.metodo_de_entrega = metodo_de_entrega
            webViewVC.importe_fijo_metodo_pago = importe_fijo_metodo_pago
            webViewVC.costo_metodo_de_pago = costo_metodo_de_pago
            webViewVC.costo_metodo_de_entrega = costo_metodo_de_entrega
            webViewVC.idMetodoPago = idMetodoPago
            webViewVC.total_fila_asiento_id = total_fila_asiento_id
            webViewVC.idFormaEntrega = idFormaEntrega
            webViewVC.index_numerado = index_numerado
            webViewVC.mensagesetFila = mensagesetFila
            webViewVC.mensagesetIniColumna = mensagesetIniColumna
            webViewVC.mensagesetFinColumna = mensagesetFinColumna
            webViewVC.idfila = idfila
            webViewVC.idZona = idZona
        }
    }
    
}
