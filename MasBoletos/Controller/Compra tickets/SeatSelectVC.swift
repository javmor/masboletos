 //
//  ThirdViewController.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 8/15/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
 
class SeatSelectVC: UIViewController, ZSeatSelectorDelegate {
    var jsonArray: NSArray?
    
    var arrayid = [Int]()
    var idevento = [Int]()
    var idzona = [Int]()
    
    var arrayfila = [String]()
    var arrayinicia = [Int]()
    var arraytermina = [Int]()
    var arraypasillo = [String]()
    
    var mensagesetMensage = String()
    var mensagesetIdZona: Any = ""
    var mensagesetFila = String()
    var mensagesetIniColumna = Int()
    var mensagesetFinColumna = Int()
    var idfila = Int()
    var mensagesetTipo = Int()
    var mensagesetAsientos = String()
    
    var map:String = ""
    
    //Variables para el pago
    var direccion_evento: String?
    var more_information: String?
    var url_header_image: String?
    var fecha_evento = String()
    var hora_evento = String()
    var nombre_evento = String()
    var total_asientos = String()
    var total_fila_asiento_id = String()
    
    @IBOutlet weak var stackViewSeatBooking: UIStackView!
    
    @IBOutlet weak var labelSeccion: UILabel!
    @IBOutlet weak var labelAsientos: UILabel!
    @IBOutlet weak var labelPrecios: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    
    //Variables from SecondViewController
    
    var checkSwitchAsientos: Int!
    var valueIDEventoFuncion: Int!
    var countTickets: Int!
    var idZona: Int!
    var precio: Int!
    var seccion: String!
    var valueEventoFuncion: Int!
    
     var index_numerado: Int!
    
    
    let nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("SIGUIENTE", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 4
        button.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        button.layer.borderColor = UIColor.white.cgColor
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupButtonNextNavBar()
        view.backgroundColor = UIColor.colorWithHexString(hexStr: "F6F6F6")
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        setupButtonNextNavBar()
    }
    
    private func setupButtonNextNavBar() {
        let buttonNavBarItem = UIBarButtonItem.init(customView: nextButton)
        nextButton.isEnabled = false
        nextButton.alpha = 0.10
        nextButton.addTarget(self, action: #selector(addTapped), for: .touchUpInside)
        navigationItem.rightBarButtonItems = [buttonNavBarItem]
    }
    
    @objc private func addTapped() {
        
        self.performSegue(withIdentifier: "segueFromBookingToFourController", sender: self)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
            
        DispatchQueue.main.async {
            self.checkSwitchAsientos = 0
            self.downloadSeats(idZona: self.idZona, idEvento: self.valueEventoFuncion)
            
            self.labelPrecios.text = "$\(self.precio!).00\t"
            self.labelSeccion.text = "\(self.seccion!)\t"
            
            let totalTickets:Int = self.countTickets
            self.labelTotal.text = "$\(self.precio! * totalTickets).00"
        }
    }
    
    func drawSeatBooking(map: String) {
        
        let seats = ZSeatSelector()
        seats.frame = CGRect(x: 0, y: self.view.frame.size.width/8, width: self.view.frame.size.width, height: self.view.frame.size.height/3)
        //seats.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 300))
        seats.setSeatSize(CGSize(width: 60, height: 56))
        seats.setRowLetter(arrayFila: arrayfila)
        seats.setAvailableImage(UIImage(named: "A")!,
                                andUnavailableImage:UIImage(named: "U")!,
                                andDisabledImage:   UIImage(named: "D")!,
                                andSelectedImage:   UIImage(named: "S")!)
        seats.layout_type = "Normal"
        seats.setMap(map)
        seats.seat_price = 10.0
        seats.selected_seat_limit = self.countTickets
        seats.seatSelectorDelegate = self
        self.stackViewSeatBooking.addArrangedSubview(seats)
        
    }
    
    func seatSelected(_ seat: ZSeat) {
        //print("Seat at row: \(seat.row) and column: \(seat.column)\n")
    }
    
    func getSelectedSeats(_ seats: NSMutableArray) {
        
        var total:Int = 0;
        var totalAsientos:String = ""
        var regExFilaAsientos: String = ""
        
        for i in 0..<seats.count {
            let seat:ZSeat  = seats.object(at: i) as! ZSeat
            print("Seat at row: \(seat.row) and column: \(seat.column)\n")
            totalAsientos += "\(arrayfila[seat.row - 1].trimmingCharacters(in: .whitespacesAndNewlines))\(seat.column),"
            regExFilaAsientos += "\(arrayfila[seat.row - 1].trimmingCharacters(in: .whitespacesAndNewlines))-\(seat.column)-\(arrayid[seat.row - 1]),"
            total += 1
            
        }
        
        totalAsientos =  String(totalAsientos.dropLast())
        labelAsientos.text = "\(totalAsientos)\t"
        total_asientos = totalAsientos
        
        regExFilaAsientos = String(regExFilaAsientos.dropLast())
        total_fila_asiento_id = regExFilaAsientos
        
        print("Total asientos: \(total_asientos)")
        print("reg-fila: \(regExFilaAsientos)")
        if total == countTickets {
            nextButton.isEnabled = true
            nextButton.alpha = 1.0
        }else{
            nextButton.isEnabled = false
            nextButton.alpha = 0.10
        }
        
    }
    
    private func downloadSeats(idZona: Int, idEvento: Int) {
        
        let urlString = "https://www.masboletos.mx/appMasboletos/getButacas.php?idevento=\(idEvento)&idzona=\(idZona)"
        Alamofire.request(urlString).responseJSON { response in
        
            if let JSON = response.result.value {
                
                self.jsonArray = JSON as? NSArray
                
                for item in self.jsonArray! as! [NSDictionary] {
                    
                    let idzona = item["id"] as! Int
                    let fila = item["fila"] as! String
                    let asientos = item["asientos"] as! String
                    
                    self.arrayid.append(idzona)
                    self.arrayfila.append(fila)
                    self.map += "\(asientos)/"
                    
                }
                
                self.drawSeatBooking(map: self.map)
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let fourthVC = segue.destination as? PaymenthMethodsVC {
            fourthVC.countTickets = countTickets
            fourthVC.valueEventoFuncion = valueEventoFuncion
            fourthVC.idZona = idZona
            fourthVC.seccion = seccion
            fourthVC.precio = precio
            fourthVC.total_asientos = total_asientos
            fourthVC.total_fila_asiento_id = total_fila_asiento_id
            fourthVC.nombre_evento = nombre_evento
            fourthVC.hora_evento = hora_evento
            fourthVC.fecha_evento = fecha_evento
            fourthVC.index_numerado = index_numerado
            fourthVC.mensagesetFila = mensagesetFila
            fourthVC.mensagesetIniColumna = mensagesetIniColumna
            fourthVC.mensagesetFinColumna = mensagesetFinColumna
            fourthVC.idfila = idfila
            
            fourthVC.direccion_evento = direccion_evento
            fourthVC.url_header_image = url_header_image
            fourthVC.more_information = more_information
    
        }
    }
}
