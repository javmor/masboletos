//
//  ShowPaymentViewController.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 8/27/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit

class SummaryPaymenthVC: UIViewController {
    
    let seccionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let asientosLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let precioLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let totalLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let informationLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = UIColor.colorWithHexString(hexStr: "545454")
        label.font = UIFont(name: "AvenirNext-Regular", size: 16)
        label.text = "Precios en Pesos Mexicanos MX$\n  Los boletos son válidos unicamente en el día y horario seleccionados. No se realizará reembolso ni cambios de evento una vez realizada la compra."
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var countTickets = Int()
    var valueEventoFuncion: Int!
    var seccion = String()
    var precio = Int() 
    var fecha_evento = String()
    var hora_evento = String()
    var nombre_evento = String()
    var total_asientos = String()
    var idZona = Int()
    var mensagesetMensage = String()
    var mensagesetIdZona: Any = ""
    var mensagesetFila = String()
    var mensagesetIniColumna = Int()
    var mensagesetFinColumna = Int()
    var idfila = Int()
    var mensagesetTipo = Int()
    var mensagesetAsientos = String()
    
    var index_numerado: Int!
    
    var fila_seleccion = String()
    
    var direccion_evento: String?
    var more_information: String?
    var url_header_image: String?
    
    let nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.layer.borderColor = UIColor.white.cgColor
        button.setTitle("SIGUIENTE", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont(name: "AvenirNext-Regular", size: 14)
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 4
        button.contentEdgeInsets = UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3)
        return button
    }()
    
    let cardView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 15.0
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 12.0
        view.layer.shadowOpacity = 0.7
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    //MARK: init
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Compra"
        setup()
    }

    //MARK: Selectors
    @objc private func addTapped() {
        print("Cliked button")
        self.performSegue(withIdentifier: "segueToFourthController", sender: self)
    }
    
    
    //MARK: Helpers
    private func showInformationPayments() {
        //let fila = mensagesetFila.trimmingCharacters(in: .whitespacesAndNewlines)
        //labelSeccion.text = "\(seccion!)"
        //labelPrecio.text = "$\(precio!).00"
        //labelAsientos.text = "\(fila)\(mensagesetAsientos)"
        //total_asientos = "\(fila)\(mensagesetAsientos)"
        //labelTotal.text = "$\(self.precio * countTickets).00"
        
        //labelInformacion.text = "Precios en Pesos Mexicanos MX$\nLos boletos son válidos unicamente en el día y horario seleccionados. No se realizará reembolso ni cambios de evento una vez realizada la compra."
        //labelInformacion.adjustsFontSizeToFitWidth = true
    }
    
    private func setup() {
        nextButton.isEnabled = true
        nextButton.alpha = 1.0
        nextButton.addTarget(self, action: #selector(addTapped), for: .touchUpInside)
        let buttonNavBarItem = UIBarButtonItem.init(customView: nextButton)
        navigationItem.rightBarButtonItems = [buttonNavBarItem]
        view.backgroundColor = UIColor.colorWithHexString(hexStr: "F6F6F6")
        
        view.addSubview(cardView)
        view.addSubview(informationLabel)
        cardView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 24, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 150))
        
        [seccionLabel, precioLabel, asientosLabel, totalLabel].forEach { cardView.addSubview($0) }
        
        seccionLabel.centerXAnchor.constraint(equalTo: cardView.centerXAnchor).isActive = true
        seccionLabel.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 16).isActive = true
        seccionLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        seccionLabel.anchor(top: nil, leading: cardView.leadingAnchor, bottom: nil, trailing: cardView.trailingAnchor, padding: .init(top: 32, left: 8, bottom: 0, right: 8))
        configureLabel(label: seccionLabel, title: "Sección: ", details: seccion)
        
        precioLabel.centerXAnchor.constraint(equalTo: cardView.centerXAnchor).isActive = true
        precioLabel.topAnchor.constraint(equalTo: seccionLabel.bottomAnchor).isActive = true
        precioLabel.heightAnchor.constraint(equalTo: seccionLabel.heightAnchor).isActive = true
        precioLabel.anchor(top: nil, leading: cardView.leadingAnchor, bottom: nil, trailing: cardView.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8))
        configureLabel(label: precioLabel, title: "Precio: ", details: convertDoubleToCourrency(amount: Double(precio)))
        
        asientosLabel.centerXAnchor.constraint(equalTo: cardView.centerXAnchor).isActive = true
        asientosLabel.topAnchor.constraint(equalTo: precioLabel.bottomAnchor).isActive = true
        asientosLabel.heightAnchor.constraint(equalTo: seccionLabel.heightAnchor).isActive = true
        asientosLabel.anchor(top: nil, leading: cardView.leadingAnchor, bottom: nil, trailing: cardView.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8))
        configureLabel(label: asientosLabel, title: "Asientos: ", details: mensagesetAsientos)
        
        let _fila = mensagesetFila.trimmingCharacters(in: .whitespacesAndNewlines)
        totalLabel.centerXAnchor.constraint(equalTo: cardView.centerXAnchor).isActive = true
        totalLabel.topAnchor.constraint(equalTo: asientosLabel.bottomAnchor).isActive = true
        totalLabel.heightAnchor.constraint(equalTo: asientosLabel.heightAnchor).isActive = true
        totalLabel.anchor(top: nil, leading: cardView.leadingAnchor, bottom: nil, trailing: cardView.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8))
        configureLabel(label: totalLabel, title: "Total asientos: ", details: "\(_fila)\(mensagesetAsientos)")
        
        informationLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        informationLabel.heightAnchor.constraint(equalToConstant: 150).isActive = true
        informationLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -32).isActive = true
        informationLabel.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 8, bottom: 0, right: 8))
        
    }

    private func configureLabel(label: UILabel, title: String, details: String) {
        let attributedText = NSMutableAttributedString(attributedString: NSAttributedString(string: "\(title)  ", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.mainBlue()]))

        attributedText.append(NSAttributedString(string: "\(details)", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.gray]))
        label.attributedText = attributedText
    }
    
    

    //MARK: Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        if segue.identifier == "segueToFourthController" {
            if let fVC = segue.destination as? PaymenthMethodsVC {
                fVC.idZona = idZona
                fVC.valueEventoFuncion = valueEventoFuncion
                fVC.countTickets = countTickets
                fVC.seccion = seccion
                fVC.precio = precio
                fVC.total_asientos = total_asientos
                fVC.nombre_evento = nombre_evento
                fVC.hora_evento = hora_evento
                fVC.fecha_evento = fecha_evento
                fVC.index_numerado = index_numerado
                
                fVC.mensagesetFila = mensagesetFila
                fVC.mensagesetIniColumna = mensagesetIniColumna
                fVC.mensagesetFinColumna = mensagesetFinColumna
                fVC.idfila = idfila
                
                fVC.direccion_evento = direccion_evento
                fVC.url_header_image = url_header_image
                fVC.more_information = more_information
                
            }
        }
        
    }
}
