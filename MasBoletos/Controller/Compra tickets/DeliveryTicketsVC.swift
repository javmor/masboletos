//
//  FifthViewController.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 9/13/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class DeliveryTicketsVC: UIViewController {

    
    var deliveryMethodArray = [DeliveryMethodModel]()
    
    var idZona = Int()
    var countTickets = Int()
    var valueEventoFuncion = Int()
    var seccion = String()
    var precio = Int()
    
    //Variables forma de pago
    var fecha_evento = String()
    var hora_evento = String()
    var nombre_evento = String()
    var direccion_evento: String?
    var more_information: String?
    var url_header_image: String?
    var total_asientos = String()
    var total_fila_asiento_id = String()
    var idfila = Int()
    var metodo_de_pago = String()
    var costo_metodo_de_pago = Double()
    var importe_fijo_metodo_pago = Int()
    var costo_metodo_de_entrega = Double()
    var metodo_de_entrega = String()
    var idMetodoPago = Int()
    var idFormaEntrega = Int()
    var totalPrecioBoletos = Int()
    var cargoTotalFormaEntrega = Double()
    var costoSeguroBoleto = Double()
    var index_numerado: Int!
    var fila_seleccion = String()
    var descripcion_seguro_boleto:String = ""
    var mensagesetIdZona: Any = ""
    var mensagesetFila = String()
    var mensagesetIniColumna = Int()
    var mensagesetFinColumna = Int()

    fileprivate let identifier = "cellEntrega"
    fileprivate let footerId = "footerId"
    
    let nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("SIGUIENTE", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 4
        button.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        button.layer.borderColor = UIColor.white.cgColor
        return button
    }()
    
    lazy var tvFormaEntrega: UITableView = {
        let tv = UITableView()
        tv.delegate = self
        tv.dataSource = self
        tv.register(DeliveryCell.self, forCellReuseIdentifier: identifier)
        tv.register(FooterView.self, forHeaderFooterViewReuseIdentifier: footerId)
        tv.separatorStyle = .none
        tv.backgroundColor = .clear
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let seccionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let cargoFormaEntregaLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let totalLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let cardView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 15.0
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 12.0
        view.layer.shadowOpacity = 0.5
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    fileprivate let padding: CGFloat = 16
    
    let seguroTicketLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let priceSeguroLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let switchSeguroBoleto: UISwitch = {
        let sw = UISwitch()
        sw.onTintColor = UIColor.colorWithHexString(hexStr: "050349")
        sw.translatesAutoresizingMaskIntoConstraints = false
        sw.isOn = false
        sw.setOn(false, animated: true)
        sw.addTarget(self, action: #selector(stateChanged(sender:)), for: .valueChanged)
        sw.isEnabled = false
        return sw
    }()
    
    let buttonSeguroBoleto: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Ver descripción", for: .normal)
        button.titleLabel?.font = UIFont(name: "AvenirNext-Regular", size: 15)
        button.setTitleColor(UIColor.mainBlue(), for: .normal)
        button.backgroundColor = UIColor.clear
        button.layer.cornerRadius = 17.5
        button.layer.borderColor = UIColor.mainBlue().cgColor
        button.layer.borderWidth = 1
        button.clipsToBounds = true
        button.tag = 99
        button.addTarget(self, action: #selector(handleSeguro(_:)), for: .touchUpInside)
        //button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let viewSegurosCard: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 15.0
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 12.0
        view.layer.shadowOpacity = 0.7
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    
    //MARK: init
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        fetchDownloadsDelivery()
        setupButtonNextNavBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let index = self.tvFormaEntrega.indexPathForSelectedRow{
            self.tvFormaEntrega.deselectRow(at: index, animated: true)
        }
    }
    
    //MARK: Selectors
    @objc func addTapped() {
        self.performSegue(withIdentifier: "segueToInitSession", sender: self)
    }
    
    @objc func stateChanged(sender: UISwitch) {
        if switchSeguroBoleto.isOn {
            cargoTotalFormaEntrega = cargoTotalFormaEntrega + costoSeguroBoleto
            configureLabel(label: cargoFormaEntregaLabel, title: "Cargo por forma de entrega: ", details: convertDoubleToCourrency(amount: cargoTotalFormaEntrega), colorDetails: UIColor.gray)
        } else {
            if cargoTotalFormaEntrega != 0.0 {
                cargoTotalFormaEntrega = cargoTotalFormaEntrega - costoSeguroBoleto
                configureLabel(label: cargoFormaEntregaLabel, title: "Cargo por forma de entrega: ", details: convertDoubleToCourrency(amount: cargoTotalFormaEntrega), colorDetails: UIColor.gray)
            }
        }
    }
    
    @objc func handleSeguro(_ sender: UIButton) {
        
        
        if sender.tag == 99 {
            alerts(title: "Seguro", menssage: self.descripcion_seguro_boleto)
        }else{
            alerts(title: deliveryMethodArray[sender.tag].texto, menssage: deliveryMethodArray[sender.tag].descripcion)
        }
        
    }
    
    //MARK: Helpers
    private func setup() {
        title = "Forma de Entrega"
        view.backgroundColor = UIColor.colorWithHexString(hexStr: "F4F4F4")
        
        //seccionLabel.text = "\(seccion) x \(countTickets)"
        //seccionTotalPago.text = "$\(precio*countTickets).00"
        totalPrecioBoletos = precio * countTickets
        
        let cardInfoEvent = InfoEvent()
        cardInfoEvent.setupLabels(title: nombre_evento.replacingOccurrences(of: "%20", with: " "), type: 1)
        cardInfoEvent.setupLabels(direction: direccion_evento, type: 2)
        cardInfoEvent.setupLabels(moreInformation: fecha_evento, type: 3)
        
        cardInfoEvent.setupImages(imageUrlBackground: URL(string: url_header_image!)!, imageUrl: URL(string: url_header_image!)!)
        
        view.addSubview(cardInfoEvent)
        cardInfoEvent.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, size: .init(width: 0, height: 150))
        
        view.addSubview(cardView)
        cardView.anchor(top: cardInfoEvent.bottomAnchor, leading: cardInfoEvent.leadingAnchor, bottom: nil, trailing: cardInfoEvent.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 135))
        
        view.addSubview(tvFormaEntrega)
        if #available(iOS 11.0, *) {
            tvFormaEntrega.anchor(top: cardView.bottomAnchor, leading: cardView.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: cardView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        } else {
            tvFormaEntrega.anchor(top: cardView.bottomAnchor, leading: cardView.leadingAnchor, bottom: view.bottomAnchor, trailing: cardView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        }

        //MARK: - cardView
        [seccionLabel, cargoFormaEntregaLabel, totalLabel].forEach { cardView.addSubview($0) }

        seccionLabel.anchor(top: cardView.topAnchor, leading: cardView.leadingAnchor, bottom: nil, trailing: cardView.trailingAnchor, padding: .init(top: 18, left: 8, bottom: 0, right: 8))
        seccionLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        configureLabel(label: seccionLabel, title: "Sección: ", details: seccion, colorDetails: UIColor.gray)

        cargoFormaEntregaLabel.anchor(top: seccionLabel.bottomAnchor, leading: seccionLabel.leadingAnchor, bottom: nil, trailing: seccionLabel.trailingAnchor, padding: .init(top: 4, left: 0, bottom: 0, right: 0))
        cargoFormaEntregaLabel.heightAnchor.constraint(equalTo: seccionLabel.heightAnchor).isActive = true
        configureLabel(label: cargoFormaEntregaLabel, title: "Cargo por forma de entrega: ", details: convertDoubleToCourrency(amount: cargoTotalFormaEntrega), colorDetails: UIColor.gray)

        totalLabel.anchor(top: cargoFormaEntregaLabel.bottomAnchor, leading: seccionLabel.leadingAnchor, bottom: nil, trailing: seccionLabel.trailingAnchor, padding: .init(top: 4, left: 0, bottom: 0, right: 0))
        totalLabel.heightAnchor.constraint(equalTo: seccionLabel.heightAnchor).isActive = true

        //let count_total = totalPrecioBoletos * countTickets
        configureLabelTitleRed(label: totalLabel, title: "Total a pagar: ", details: convertDoubleToCourrency(amount: Double(totalPrecioBoletos)))
        
        view.addSubview(viewSegurosCard)
        if #available(iOS 11.0, *) {
            viewSegurosCard.anchor(top: nil, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing:
                view.trailingAnchor, padding: .init(top: 0, left: 8, bottom: 8, right: 8),size: .init(width: 0, height: 100))
        } else {
            viewSegurosCard.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing:
                view.trailingAnchor, padding: .init(top: 0, left: 8, bottom: 8, right: 8),size: .init(width: 0, height: 100))
        }


        [seguroTicketLabel, switchSeguroBoleto, buttonSeguroBoleto].forEach { viewSegurosCard.addSubview($0) }

        seguroTicketLabel.anchor(top: viewSegurosCard.topAnchor, leading: viewSegurosCard.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 8, left: 8, bottom: 0, right: 0), size: .init(width: view.frame.size.width - 120, height: 35))


        switchSeguroBoleto.anchor(top: seguroTicketLabel.topAnchor, leading: seguroTicketLabel.trailingAnchor, bottom: nil, trailing: viewSegurosCard.trailingAnchor, padding: .init(top: 0, left: 8, bottom: 0, right: 0))
        switchSeguroBoleto.heightAnchor.constraint(equalTo: seguroTicketLabel.heightAnchor).isActive = true

        buttonSeguroBoleto.anchor(top: nil, leading: viewSegurosCard.leadingAnchor, bottom: viewSegurosCard.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 8, bottom: 12, right: 16), size: .init(width: 150, height: 35))
        
    }
    
    private func configureLabel(label: UILabel, title: String, details: String, colorDetails: UIColor) {
        let attributedText = NSMutableAttributedString(attributedString: NSAttributedString(string: "\(title)  ", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.mainBlue()]))
        
        attributedText.append(NSAttributedString(string: "\(details)", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: colorDetails]))
        label.attributedText = attributedText
        
    }
    
    private func configureLabelTitleRed(label: UILabel, title: String, details: String) {
        let attributedText = NSMutableAttributedString(attributedString: NSAttributedString(string: "\(title)  ", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.mainRed()]))
        
        attributedText.append(NSAttributedString(string: "\(details)", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.mainBlue()]))
        label.attributedText = attributedText
        
    }

    
    private func setupButtonNextNavBar() {
        let buttonNavBarItem = UIBarButtonItem.init(customView: nextButton)
        nextButton.isEnabled = false
        nextButton.alpha = 0.10
        nextButton.addTarget(self, action: #selector(addTapped), for: .touchUpInside)
        navigationItem.rightBarButtonItems = [buttonNavBarItem]
    }
    
    private func fetchDownloadsDelivery() {
        let urlString = "https://www.masboletos.mx/appMasboletos/getFormaPagoFormaEntrega.php?idevento=\(valueEventoFuncion)"
        Alamofire.request(urlString).responseJSON { response in
            switch response.result {
            case .success:
                do {
                    guard let datas = response.data else { return }
                    let json = try JSON(data: datas)
                    let json_datas = json.arrayValue
                    
                    for item in json_datas {
                        let type = item["Tipo"].stringValue
                        if type == "FormaEntrega" {
                            let _idTipo = item["idforma"].intValue
                            if _idTipo == 1 {
                                let costo = item["Costo"].stringValue
                                let text = item["texto"].stringValue
                                let description = item["descripcion"].stringValue
                                self.descripcion_seguro_boleto = description
                                self.setInformationTextEnsure(title: text, details: costo)
                                //self.deliveryMethodArray.append(DeliveryMethodModel(idforma: _idTipo, porcentajecargo: porcentajeCargo, costo: costo, texto: text, descripcion: description))
                                self.viewSegurosCard.isHidden = false
                            }else{
                                let costo = item["Costo"].stringValue
                                let text = item["texto"].stringValue
                                let description = item["descripcion"].stringValue
                                let porcentajeCargo = item["porcentajecargo"].intValue
                                self.deliveryMethodArray.append(DeliveryMethodModel(idforma: _idTipo, porcentajecargo: porcentajeCargo, costo: costo, texto: text, descripcion: description))
                            }
                        }
                    }
                    self.tvFormaEntrega.reloadData()
                } catch {
                    print("Error SwiftyJSON")
                }
            case .failure(let error):
                print("Error: \(error)")
            }
        }
    }
    
    private func setInformationTextEnsure(title: String, details: String) {
        let aux_costo = Double(details)
        configureLabel(label: seguroTicketLabel, title: title, details: convertDoubleToCourrency(amount: aux_costo!), colorDetails: UIColor.mainRed())
        viewSegurosCard.isHidden = true
        costoSeguroBoleto = aux_costo!
    }
    
    private func alerts(title: String, menssage: String) {
        let alert = UIAlertController(title: "\(title)", message: "\(menssage)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueToInitSession" {
            if let rpVC = segue.destination as? RevisionPagoVC {
                
                rpVC.valueEventoFuncion = valueEventoFuncion
                rpVC.nombre_evento = nombre_evento
                rpVC.hora_evento = hora_evento
                rpVC.fecha_evento = fecha_evento
                rpVC.seccion = seccion
                rpVC.precio = precio
                rpVC.countTickets = countTickets
                rpVC.total_asientos = total_asientos
                rpVC.total_fila_asiento_id = total_fila_asiento_id
                rpVC.metodo_de_pago = metodo_de_pago
                rpVC.metodo_de_entrega = metodo_de_entrega
                rpVC.importe_fijo_metodo_pago = importe_fijo_metodo_pago
                rpVC.costo_metodo_de_pago = costo_metodo_de_pago
                rpVC.costo_metodo_de_entrega = costo_metodo_de_entrega
                rpVC.idMetodoPago = idMetodoPago
                rpVC.idFormaEntrega = idFormaEntrega
                rpVC.index_numerado = index_numerado
                rpVC.mensagesetFila = mensagesetFila
                rpVC.mensagesetIniColumna = mensagesetIniColumna
                rpVC.mensagesetFinColumna = mensagesetFinColumna
                rpVC.idfila = idfila
                rpVC.idZona = idZona
                
            }
        }
        
    }
}

//MARK: Extensions
extension DeliveryTicketsVC:  UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deliveryMethodArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! DeliveryCell
        
        cell.initLabelWithText(title: deliveryMethodArray[indexPath.row].texto, price: convertDoubleToCourrency(amount: Double(deliveryMethodArray[indexPath.row].costo)!), tagB: indexPath.row)
        
        cell.button.addTarget(self, action: #selector(handleSeguro(_:)), for: .touchUpInside)
        cell.button.tag = indexPath.row
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        nextButton.isEnabled = true
        nextButton.alpha = 1.0

        switchSeguroBoleto.isEnabled = true
        metodo_de_entrega = deliveryMethodArray[indexPath.row].texto
        idFormaEntrega = deliveryMethodArray[indexPath.row].idforma
        costo_metodo_de_entrega = Double(deliveryMethodArray[indexPath.row].costo)!

        guard let costo = Double(deliveryMethodArray[indexPath.row].costo) else { return }
        let total = (costo * Double(countTickets))
        
        configureLabel(label: cargoFormaEntregaLabel, title: "Cargo por forma de entrega: ", details: convertDoubleToCourrency(amount: total), colorDetails: .gray)
        cargoTotalFormaEntrega = Double(total)
        
        let totalPagoWithFormaEntrega = total + Double(totalPrecioBoletos)
        configureLabelTitleRed(label: totalLabel, title: "Total a pagar: ", details: convertDoubleToCourrency(amount: totalPagoWithFormaEntrega))
        
        //cargoFormaEntregalbl.text = "$\(costo! * Double(countTickets))0"
        //totalPago.text = "$\(total+Double(totalPrecioBoletos))0"
    }
    
}



class DeliveryCell: UITableViewCell {
    
    let cellView: UIView = {
        let view = UIView()
        view.setCellShadow()
        view.backgroundColor = .white
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "AvenirNext-Medium", size: 15)
        label.textColor = UIColor.mainBlue()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "AvenirNext-Medium", size: 15)
        label.textColor = UIColor.mainBlue()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let button: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Ver descripción", for: .normal)
        button.setTitleColor(UIColor.mainBlue(), for: .normal)
        button.backgroundColor = UIColor.clear
        button.layer.cornerRadius = 15
        button.layer.borderColor = UIColor.mainBlue().cgColor
        button.layer.borderWidth = 1
        button.translatesAutoresizingMaskIntoConstraints = false
        button.clipsToBounds = true
        return button
    }()
    
    func setup() {
        backgroundColor = UIColor(r: 245, g: 245, b: 245)
        
        addSubview(cellView)
        cellView.fillSuperview(padding: .init(top: 8, left: 8, bottom: 8, right: 8))
        
        [titleLabel, priceLabel, button].forEach { cellView.addSubview($0) }
        
        titleLabel.topAnchor.constraint(equalTo: cellView.topAnchor, constant: 8).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: cellView.leadingAnchor, constant: 8).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: cellView.trailingAnchor, constant: -8).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        priceLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8).isActive = true
        priceLabel.leadingAnchor.constraint(equalTo: cellView.leadingAnchor, constant: 8).isActive = true
        priceLabel.heightAnchor.constraint(equalTo: titleLabel.heightAnchor).isActive = true
        
        button.topAnchor.constraint(equalTo: priceLabel.topAnchor).isActive = true
        button.leadingAnchor.constraint(equalTo: priceLabel.trailingAnchor).isActive = true
        button.trailingAnchor.constraint(equalTo: cellView.trailingAnchor, constant: -8).isActive = true
        button.widthAnchor.constraint(equalToConstant: 150).isActive = true
        button.heightAnchor.constraint(equalTo: priceLabel.heightAnchor).isActive = true
        
    }
    
    func initLabelWithText(title: String, price: String, tagB: Int) {
        titleLabel.text = title
        priceLabel.text = price
        button.tag = tagB
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

