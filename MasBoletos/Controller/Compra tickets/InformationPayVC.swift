//
//  InformationPayViewController.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 9/28/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit

class InformationPayVC: UIViewController {
    
    @IBOutlet weak var InformationNamelbl: UILabel!
    @IBOutlet weak var transaction_paymentlbl: UILabel!
    @IBOutlet weak var folio_paymentlbl: UILabel!
    
    var id_transaccion = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLabelsInformation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = true
    }
    
    private func setLabelsInformation() {
        InformationNamelbl.text = "\(UserDefaults.standard.getUserName()) SU COMPRA SE HA REALIZADO CORRECTAMENTE"
        InformationNamelbl.layer.addBorder(edge: UIRectEdge.bottom, color: UIColor.gray, thickness: 1.0)
        transaction_paymentlbl.text = "\(id_transaccion)"
        transaction_paymentlbl.layer.addBorder(edge: UIRectEdge.bottom, color: UIColor.gray, thickness: 1.0)
    }
    
    @IBAction func btn_continue_shop(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btn_show_tickets(_ sender: Any) {
        //self.navigationController?.popToViewController(, animated: true)
    }
}

