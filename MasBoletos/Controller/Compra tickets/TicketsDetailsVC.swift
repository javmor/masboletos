//
//  DetailViewController.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 8/7/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit
import Alamofire
import AZDialogView
import SwiftyJSON
import VisualEffectView
import SDWebImage

class TicketsDetailsVC: UIViewController {
    
    @IBOutlet weak var imageViewLeft: UIImageView!
    @IBOutlet weak var imageViewRight: UIImageView!
    
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var SecondLabel: UILabel!
    @IBOutlet weak var ThirdLabel: UILabel!
    
    @IBOutlet weak var stackViewPicker: UIStackView!
    @IBOutlet weak var cantidadBoletos: UILabel!
    @IBOutlet weak var datePickerDate: UIPickerView!
    
    var countTickets = Int()
    var nombre_evento: String?
    var direccion_evento: String?
    var more_information: String?
    var url_header_image: String?
    var url_image_left: String?
    var lugar: String?
    
    var idEvento: Int?
    var imagenEvento: String!
    var eventoGrupo: Int!
    var valueEventoFuncion: Int?
    var fecha_evento = String()
    var hora_evento = String()
    
    var index: Int!
    var arrayHoraEvento = [String]()
    
    var arrayFuncionesEventos = [ModelFuncionesEventos]()
    var arrayEventoEncabezado = [ModelEventoEncabezado]()

    var moreInformationAlert = String()
    
    let nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.addSettingsButtonBarNextEnabled()
        return button
    }()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        title = "Boletos"
        hiddenOrNotPicker()
        //setupBottomControls()
        setupNavegationItems()
        datePickerDate.dataSource = self
        datePickerDate.delegate = self
        setLabelMoreInformation()
        view.backgroundColor = UIColor.colorWithHexString(hexStr: "F6F6F6")
    
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        print(hora_evento)
        print(fecha_evento)
        
    }

    private func setLabelMoreInformation() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(labelTapped))
        ThirdLabel.isUserInteractionEnabled = true
        ThirdLabel.addGestureRecognizer(tap)
        ThirdLabel.text = "Más Información..."
        
    }
    
    @objc func labelTapped() {
        let dialog = AZDialogViewController(title: "\(firstLabel.text!)", message: "\(moreInformationAlert)")
        self.present(dialog, animated: false, completion: nil)
    }
    
    override func didReceiveMemoryWarning() { super.didReceiveMemoryWarning() }
    
    @IBAction func stepper(_ sender: UIStepper) {
        
        cantidadBoletos.text = String(Int(sender.value))
        countTickets = Int(sender.value)
        
        if sender.value == 0.0 {
            nextButton.isEnabled = false
            nextButton.alpha = 0.10
        }else{
            nextButton.alpha = 1.0
            nextButton.isEnabled = true
        }
        
    }

    private func hiddenOrNotPicker() {
        showHUD(progressLabel: "Cargando")
        
        if eventoGrupo == 0 {
            stackViewPicker.isHidden = true
            valueEventoFuncion = idEvento
            setImagesAndLabels()
        } else {
            valueEventoFuncion = idEvento
            downloadAndSetPickerDates(evento_id: eventoGrupo)
            setImagesAndLabels()
        }
    }
    
    private func setupNavegationItems() {
        let action = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(addTapped))
        let buttonNavBarItem = UIBarButtonItem.init(customView: nextButton)
        nextButton.isEnabled = false
        nextButton.alpha = 0.10
        nextButton.addTarget(self, action: #selector(onButtonNextview), for: .touchUpInside)
        navigationItem.rightBarButtonItems = [buttonNavBarItem,action]
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    @objc private func addTapped() {
        
        let urlToShared = URL(string: "http://www.masboletos.mx/evento.php?idevento=\(valueEventoFuncion!)")
        let activityViewController = UIActivityViewController(activityItems: [urlToShared!],
                                                              applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.view
        present(activityViewController, animated: true, completion: nil)
        
    }
    
    @objc private func onButtonNextview() {
        self.performSegue(withIdentifier: "segueToSecondViewController", sender: self)
    }
    

    private func downloadAndSetPickerDates(evento_id: Int) {
        
        let urlString = "http://www.masboletos.mx/appMasboletos/getFuncionesxEvento.php?eventogrupo=\(evento_id)"
        print(urlString)
        Alamofire.request(urlString).responseJSON { response in
            switch response.result {
            case .success:
              
                do {
                    let jsonData = try JSON(data: response.data!)
                    let arrayResult = jsonData.arrayValue
                    
                    for items in arrayResult {
                        
                        let aux_idevento_funcion = items["idevento_funcion"].intValue
                        let FechaLarga = items["FechaLarga"].stringValue
                        let hora = items["hora"].stringValue
                        
                        self.arrayFuncionesEventos.append(ModelFuncionesEventos(idevento_funcion: aux_idevento_funcion, FechaLarga: FechaLarga, hora: hora, fecha_completa: "\(FechaLarga) \(hora)"))
                    }
                    
                    self.datePickerDate.reloadAllComponents()
                    self.dismissHUD(isAnimated: true)
                    
                }catch{
                    print("Error JSON")
                }
                
            case .failure(let error):
                let dialog = AZDialogViewController(title: "Error", message: "Evento no disponible para su venta.")
                self.present(dialog, animated: false, completion: nil)
                self.dismissHUD(isAnimated: true)
                self.navigationController?.popViewController(animated: true)
                
                print("Error evento: \(error)")
            }
        }
    }

    // https://www.masboletos.mx/appMasboletos/getEventoEncabezado.php?idevento=1348
    private func setImagesAndLabels() {
        
        let urlString = "https://www.masboletos.mx/appMasboletos/getEventoEncabezado.php?idevento=\(valueEventoFuncion!)"
        print(urlString)
        Alamofire.request(urlString).responseJSON { response in
            
            switch response.result {
            case .success:
                do{
                    let jsonData = try JSON(data: response.data!)
                    let jsonResult = jsonData.arrayValue
                    
                    for item in jsonResult {
                        let imagen = item["imagen"].stringValue
                        let direccion = item["direccion"].stringValue
                        let evento = item["evento"].stringValue
                        let lugar = item["lugar"].stringValue
                        let FechaLarga = item["FechaLarga"].stringValue
                        let hora = item["hora"].stringValue
                        let descripcion = item["descripcion"].stringValue
                        let EventoMapa = item["EventoMapa"].stringValue
                        
                        self.arrayEventoEncabezado.append(ModelEventoEncabezado(imagen: imagen, direccion: direccion, evento: evento, lugar: lugar, FechaLarga: FechaLarga, hora: hora, descripcion: descripcion, EventoMapa: EventoMapa))
                    }
                    self.setBlurView()
                    
                }catch{
                    print("Error JSON")
                }
            case .failure(let error):
                let dialog = AZDialogViewController(title: "Error", message: "Evento no disponible para su venta.")
                self.present(dialog, animated: false, completion: nil)
                self.dismissHUD(isAnimated: true)
                self.navigationController?.popViewController(animated: true)
                print("Error: \(error)")
            }
        }
    }
    
    private func setBlurView() {
        
        firstLabel.text = arrayEventoEncabezado[0].lugar
        firstLabel.numberOfLines = 0
        firstLabel.sizeToFit()
        
        SecondLabel.text = arrayEventoEncabezado[0].direccion
        SecondLabel.numberOfLines = 2
        SecondLabel.sizeToFit()
        
        moreInformationAlert = arrayEventoEncabezado[0].descripcion
        
        let image = removeSpace(str: arrayEventoEncabezado[0].imagen)
        self.imageViewLeft.sd_setImage(with: URL(string: image))
        self.imageViewRight.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "logo_masboletos"))
        
        let visualEffectView = VisualEffectView(frame: CGRect(x: 0, y: 0, width: imageViewRight.frame.width, height: imageViewLeft.bounds.height + 3))
        
        // Configure the view with tint color, blur radius, etc
        visualEffectView.colorTint = .white
        visualEffectView.colorTintAlpha = 0.1
        visualEffectView.blurRadius = 10
        visualEffectView.scale = 1
        
        self.imageViewRight.addSubview(visualEffectView)
        
        hora_evento = arrayEventoEncabezado[0].hora
        fecha_evento = arrayEventoEncabezado[0].FechaLarga
        direccion_evento = arrayEventoEncabezado[0].direccion
        more_information = arrayEventoEncabezado[0].descripcion
        url_header_image = arrayEventoEncabezado[0].imagen
        
        dismissHUD(isAnimated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToSecondViewController" {
            if let secondVC = segue.destination as? SectionZoneVC {
                secondVC.valueEventoFuncion = valueEventoFuncion
                secondVC.countTickets = countTickets
                secondVC.fecha_evento = fecha_evento
                secondVC.hora_evento = hora_evento
                secondVC.direccion_evento = direccion_evento
                secondVC.more_information = more_information
                secondVC.url_header_image = url_header_image
                secondVC.nombre_evento = nombre_evento!
            }
        }
    }
}

extension TicketsDetailsVC: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        valueEventoFuncion = arrayFuncionesEventos[row].idevento_funcion
        fecha_evento = arrayFuncionesEventos[row].FechaLarga
        hora_evento = arrayFuncionesEventos[row].hora
        return "\(arrayFuncionesEventos[row].fecha_completa)"
    }
}

extension TicketsDetailsVC: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayFuncionesEventos.count
    }
}
