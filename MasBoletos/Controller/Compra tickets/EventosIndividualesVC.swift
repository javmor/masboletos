//
//  EventosIndividualesVC.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/11/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

import UIKit
import MBProgressHUD
import NVActivityIndicatorView
import SwiftyJSON
import ViewAnimator
import SDWebImage

class EventosIndividualesVC: UIViewController {

    @IBOutlet weak var cvEventos: UICollectionView!
    //MARK: Variables identifier
    let identifier = "ItemCell"
    private var animations = [AnimationType.from(direction: .bottom, offset: 50.0)]
    
    //MARK: Components
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor(red: 5.0/255, green: 3.0/255, blue: 73.0/255, alpha: 1.0)
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        return refreshControl
    }()
    
    var eventosIndividualesFeed: [IndividualesModel]?
    
    //MARK: init
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        downloadDatasFromWS()
    }
    
    
    //MARK: Helpers
    private func setup() {
        cvEventos.delegate = self
        cvEventos.dataSource = self
        cvEventos.backgroundColor = UIColor.colorWithHexString(hexStr: "F6F6F6")
        setRefreshCollectionView()
    }
    
    
    
    private func downloadDatasFromWS() {
        
        eventosIndividualesFeed?.removeAll()
        showHUD(progressLabel: "Cargando")
        let urlString = "http://www.masboletos.mx/appMasboletos/getEventsActivesIOS.php"
        
        Service.shared.fetchGenericData(urlString: urlString, method: .get) { (eventoIndFeed: [IndividualesModel]) in
            self.eventosIndividualesFeed = eventoIndFeed
            print(eventoIndFeed)
            self.cvEventos.reloadData()
            self.cvEventos.performBatchUpdates({
                UIView.animate(views: self.cvEventos.orderedVisibleCells,
                               animations: self.animations, completion: {
                                
                })
            }, completion: nil)
            
            self.dismissHUD(isAnimated: true)
        }
    }
    
    private func setRefreshCollectionView() {
        cvEventos.alwaysBounceVertical = true
        
        if #available(iOS 10.0, *) {
            cvEventos.refreshControl = refresher
        } else {
            cvEventos.addSubview(refresher)
        }
    }
    
    @objc func refresh(sender:AnyObject) {
        
        let deadline = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.refresher.endRefreshing()
            self.cvEventos.reloadData()
            self.cvEventos.performBatchUpdates({
                UIView.animate(views: self.cvEventos.orderedVisibleCells,
                               animations: self.animations, completion: {
                                
                })
            }, completion: nil)
        }
    }
}

extension EventosIndividualesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return eventosIndividualesFeed?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! SeriesCollectionViewCell
        cell.layer.cornerRadius = 10
        cell.imageView.sd_setImage(with: URL(string: (self.eventosIndividualesFeed?[indexPath.row].imagen)!), placeholderImage: #imageLiteral(resourceName: "logo_masboletos"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let details = storyboard.instantiateViewController(withIdentifier: "sbSellFirst") as! TicketsDetailsVC
        details.nombre_evento = eventosIndividualesFeed?[(indexPath.row)].evento.replacingOccurrences(of: " ",with: "%20")
        details.idEvento = eventosIndividualesFeed?[(indexPath.row)].idevento
        details.eventoGrupo = eventosIndividualesFeed?[(indexPath.row)].eventogrupo
        navigationController?.pushViewController(details, animated: true)
        
    }
    
    //MARK: FlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt: IndexPath) -> CGSize {
        
        if collectionView == self.cvEventos {
            let numberOfColumns:CGFloat = 2
            let width = collectionView.frame.size.width
            let xInsets: CGFloat = 10
            let cellSpacing: CGFloat = 5
            
            return CGSize(width: (width / numberOfColumns) - (xInsets + cellSpacing), height: (width / numberOfColumns) - (xInsets + cellSpacing))
        } else {
            return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
        }
    }
}
