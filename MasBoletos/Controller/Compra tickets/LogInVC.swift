//
//  LogInViewController.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 9/25/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit
import Alamofire
import SkyFloatingLabelTextField
import AZDialogView

class LogInVC: UIViewController, PayPalPaymentDelegate {

    @IBOutlet weak var textFieldEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldPassword: SkyFloatingLabelTextField!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var forgotPasswordLabel: UIButton!
    @IBOutlet weak var createAccountLabel: UILabel!
    @IBOutlet weak var informationAddsv: UIStackView!
    
    var idUser = Int()
    var idZona = Int()
    var countTickets = Int()
    var valueEventoFuncion = Int()
    var seccion = String()
    
    var fecha_evento = String()
    var hora_evento = String()
    var nombre_evento = String()
    var total_asientos = String()
    var total_fila_asiento_id = String()
    var metodo_de_pago = String()
    var metodo_de_entrega = String()
    var precio = Int()
    
    var costo_metodo_de_pago = Double()
    var importe_fijo_metodo_pago = Int()
    var costo_metodo_de_entrega = Double()
    
    var idMetodoPago = Int()
    var idFormaEntrega = Int()
    var idfila = Int()
    var id_usuario = Int()
    var id_transaccion = Int()
    var id_fe = Int()
    var mensagesetIdZona: Any = ""
    var mensagesetFila = String()
    var mensagesetIniColumna = Int()
    var mensagesetFinColumna = Int()
    
    var index_numerado = Int()
    
    var payPalConfig = PayPalConfiguration()
    
    var totalTodo = Double()
    
    var loginFeed: [Login]?
    
    var environment:String = PayPalEnvironmentSandbox {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setup()
        paypalConfigure()
    }
    
    //MARK: Selectors
    @IBAction func btnCreateAccount(_ sender: UIButton) {
        if let requestUrl = URL(string: "https://www.masboletos.mx/crearperfil.php") {
            UIApplication.shared.open(requestUrl, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func btnInitSession(_ sender: UIButton) {
        showHUD(progressLabel: "Cargando")
        let _email = textFieldEmail.text!
        let _password = textFieldPassword.text!

        guard textFieldEmail.text?.count != 0  else {
            dismissHUD(isAnimated: true)
            alerts(title: "", menssage: "Por favor ingresa tu email")
            return
        }
        
        if isValidEmail(emailID: _email) == false {
            dismissHUD(isAnimated: true)
            alerts(title: "", menssage: "Ingresa una dirección de correo válida")
        }
        
        guard textFieldPassword.text?.count != 0  else {
            dismissHUD(isAnimated: true)
            alerts(title: "", menssage: "Por favor ingresa tu contraseña")
            return
        }
        
        let params: [String : String] = [
            "correo": _email,
            "contrasenia": _password,
            "tipo": "1"
        ]
        
        checkUser(params: params)
        
    }
    
    fileprivate func setup() {
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.layer.cornerRadius = 20
        loginButton.clipsToBounds = true
        loginButton.backgroundColor = .mainBlue()
        
        signupButton.translatesAutoresizingMaskIntoConstraints = false
        signupButton.layer.cornerRadius = 20
        signupButton.clipsToBounds = true
        signupButton.backgroundColor = .mainBlue()
        
        textFieldEmail.addDoneButtonOnKeyboard()
        textFieldPassword.addDoneButtonOnKeyboard()
        textFieldPassword.isSecureTextEntry = true
    }
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            
            self.setPaymentPaypal(error: 0)
            self.performSegue(withIdentifier: "segueToCompra", sender: self)
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
        })
    }
    
    private func paypalConfigure() {
        payPalConfig.acceptCreditCards = false;
        payPalConfig.merchantName = "+Boletos"
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.sivaganesh.com/privacy.html")! as URL
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.sivaganesh.com/useragreement.html")! as URL
        payPalConfig.languageOrLocale = "es_MX"
        payPalConfig.payPalShippingAddressOption = .payPal;
        PayPalMobile.preconnect(withEnvironment: environment)
    }
    
    fileprivate func setPayPayPal() {
        
        let amount = NSDecimalNumber(string: "\(totalTodo)")
        
        let payment = PayPalPayment()
        payment.amount = amount
        payment.currencyCode = "MXN"
        payment.shortDescription = "\(nombre_evento.replacingOccurrences(of: "%20", with: " "))"
        payment.intent = .sale
        
        
        if (payment.processable) {
            
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self as PayPalPaymentDelegate)
            present(paymentViewController!, animated: true, completion: nil)
        } else {
            self.setPaymentPaypal(error: 0)
            print("Pago no procesado.\(payment)")
        }
    }
    
    fileprivate func isValidEmail(emailID:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailID)
    }
    
    fileprivate func checkUser(params: [String:String]) {
        
        let urlString = "https://www.masboletos.mx/appMasboletos/validalogin.php"
        let headers = [ "Content-Type" : "application/x-www-form-urlencoded"]
        
        Service.shared.fetchGenericData(urlString: urlString, params: params,headers: headers, method: .post) { (login: [Login]) in
            self.loginFeed = login
            print(login)
            login.forEach{ (items) in
                
                if items.respuesta {
                    UserDefaults.standard.setUserID(value: items.id_cliente)
                    UserDefaults.standard.setUserName(value: items.usuario)
                    UserDefaults.standard.setLoggedIn(value: true)
                    self.whosMethodPayIs()
                    self.dismissHUD(isAnimated: true)
                }else{
                    self.dismissHUD(isAnimated: true)
                    let dialog = AZDialogViewController(title: "Error", message: "Usuario o contraseña incorrectos")
                    dialog.show(in: self)
                }
            }
        }
        
    }
    
    fileprivate func whosMethodPayIs() {
    
        if idMetodoPago == 1 {
            let dialog = AZDialogViewController(title: "Error", message: "No disponible por el momento")
            dialog.addAction(AZDialogAction(title: "Regresar") { (dialog) -> (Void) in
                self.navigationController?.popToRootViewController(animated: true)
                dialog.dismiss()
            })
            dialog.show(in: self)
        }
        
        if idMetodoPago == 2 || idMetodoPago == 3 {
            self.performSegue(withIdentifier: "segueToWebViewFromLVC", sender: self)
        }
        
        if idMetodoPago == 5 {
            pulledApartTicketsPayPal()
            setPayPayPal()
        }
        
    }
    
    fileprivate func pulledApartTicketsPayPal() {
        
        var aux_string = ""
        mensagesetFila = mensagesetFila.replacingOccurrences(of: " ", with: "")
        
        if idfila == 0 {
            aux_string = ""
        }else{
            aux_string = String(idfila)
            total_asientos = ""
        }
        
        let urlString = "https://www.masboletos.mx/masBoletosEnviaDatosPaypalMovil.php?idevento=\(valueEventoFuncion)&numerado=\(index_numerado)&cantidad=\(countTickets)&cargoxservicio=\(costo_metodo_de_pago+costo_metodo_de_entrega)&zona=\(idZona)&idcliente=\(id_usuario)&formadepago=\(idMetodoPago)&txtformaentrega=\(idFormaEntrega)&importe=\(precio)&idfila=\(aux_string)&inicolumna=\(mensagesetIniColumna)&fincolumna=\(mensagesetFinColumna)&fila=\(mensagesetFila)&idfilafilaasiento=\(total_fila_asiento_id)&filaasientos=\(total_asientos)"
        
        Alamofire.request(urlString,method: .get) .responseJSON { response in
            print(urlString)
            if let transaction = response.result.value {
                self.id_transaccion = transaction as! Int
                print(self.id_transaccion)
            }
        }
    }
    
    fileprivate func setPaymentPaypal (error: Int) {
        
        let urlString = "https://www.masboletos.mx/masMoletosRecibeDatosPaypalMovil.php?EM_OrderID=\(id_transaccion)&error=\(error)"
        
        print(urlString)
        Alamofire.request(urlString, method: .get).responseJSON { response in
            if let transaction = response.result.value {
                self.id_fe = transaction as! Int
                self.sendEmail()
            }
        }
        
    }
    
    fileprivate func sendEmail() {
        
        let urlString = "https://www.masboletos.mx/sica/masmail.cfm?trans=\(id_transaccion)&fe=\(id_fe)&de=2"
        print(urlString)
        
        Alamofire.request(urlString, method: .get).responseJSON { response in
            if let transaction = response.result.value {
                print("NO MAMES: \(transaction)")
                print("Se ha enviado el email.")
            }
        }
        
    }
    
    fileprivate func alerts(title: String, menssage: String) {
        let alert = UIAlertController(title: "\(title)", message: "\(menssage)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        if let infoVC = segue.destination as? InformationPayVC {
            infoVC.id_transaccion = id_transaccion
        }
        
        if let webViewVC = segue.destination as? WebiViewPayVC {
            webViewVC.idZona = idZona
            webViewVC.idMetodoPago = idMetodoPago
            webViewVC.idFormaEntrega = idFormaEntrega
            webViewVC.costo_metodo_de_pago = costo_metodo_de_pago
            webViewVC.totalTodo = totalTodo
            webViewVC.nombre_evento = nombre_evento
            webViewVC.valueEventoFuncion = valueEventoFuncion
            webViewVC.nombre_evento = nombre_evento
            webViewVC.hora_evento = hora_evento
            webViewVC.fecha_evento = fecha_evento
            webViewVC.seccion = seccion
            webViewVC.precio = precio
            webViewVC.countTickets = countTickets
            webViewVC.total_asientos = total_asientos
            webViewVC.metodo_de_pago = metodo_de_pago
            webViewVC.metodo_de_entrega = metodo_de_entrega
            webViewVC.importe_fijo_metodo_pago = importe_fijo_metodo_pago
            webViewVC.costo_metodo_de_pago = costo_metodo_de_pago
            webViewVC.costo_metodo_de_entrega = costo_metodo_de_entrega
            webViewVC.idMetodoPago = idMetodoPago
            webViewVC.total_fila_asiento_id = total_fila_asiento_id
            webViewVC.idFormaEntrega = idFormaEntrega
            webViewVC.index_numerado = index_numerado
            webViewVC.mensagesetFila = mensagesetFila
            webViewVC.mensagesetIniColumna = mensagesetIniColumna
            webViewVC.mensagesetFinColumna = mensagesetFinColumna
            webViewVC.idfila = idfila
            webViewVC.idZona = idZona
        }
        
    }
}

