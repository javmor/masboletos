//
//  FourthViewController.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 9/10/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit
import AZDialogView
import Alamofire
import SwiftyJSON
import SDWebImage

class PaymenthMethodsVC: UIViewController {
    
    var idZona = Int()
    var countTickets = Int()
    var valueEventoFuncion = Int()
    var seccion = String()
    var precio: Int!
    
    let bandCellId = "bandCellId"
    
    @IBOutlet weak var tableView: UITableView!
    
    var fecha_evento = String()
    var hora_evento = String()
    var nombre_evento = String()
    var total_asientos = String()
    var metodo_de_pago = String()
    var idMetodoPago = Int()
    var costo_metodo_de_pago = Double()
    var importe_fijo_metodo_pago = Int()
    var fila_seleccion = String()
    var idfila = Int()
    var mensagesetIdZona: Any = ""
    var mensagesetFila = String()
    var mensagesetIniColumna = Int()
    var mensagesetFinColumna = Int()
    var total_fila_asiento_id = String()
    
    var index_numerado: Int!
    var direccion_evento: String?
    var more_information: String?
    var url_header_image: String?
    
    var datasImages: [UIImage] = [
        UIImage(named: "puntoventa.png")!,
        UIImage(named: "paypal_icon.png")!,
        UIImage(named: "acceptance marks.png")!,
        UIImage(named: "oxxo.png")!
    ]
    
    var paymenthFeed: [PaymenthModel]?
    var paymentMethodsArray = [PaymentMethodsModel]()
    
    //MARK: init
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        fetchPaymentMethods()
    }
    
    //MARK: helpers
    private func setupTableView() {
        title = "Métodos de pago"
        navigationController?.navigationBar.shadowImage = UIImage()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.colorWithHexString(hexStr: "F6F6F6")
        tableView.backgroundColor = .clear
        tableView.register(BandCell.self, forCellReuseIdentifier: bandCellId)
    }
    
    
    private func fetchPaymentMethods() {
        let urlString = "https://www.masboletos.mx/appMasboletos/getFormaPagoFormaEntrega.php?idevento=\(valueEventoFuncion)"
        print(urlString)
        
        Alamofire.request(urlString).responseJSON { response in
            switch response.result {
            case .success:
                do {
                    let jsonDatas = try JSON(data: response.data!)
                
                    let datas = jsonDatas.arrayValue
                    print(datas)
                    for elements in datas {
                        let type = elements["Tipo"].stringValue
                        
                        if type == "FormaPago" {
                            let idTipoPago = elements["IdTipoPago"].intValue
                            let porcentajecargo = elements["porcentajecargo"].intValue
                            let texto = elements["texto"].stringValue
                            let importeFijo = elements["ImporteFijo"].intValue
                            
                            self.paymentMethodsArray.append(PaymentMethodsModel(idTipoPago: idTipoPago, porcentajeCargo: porcentajecargo, texto: texto, importeFijo: importeFijo))
                        }
                        
                    }
                
                    self.checkArrayIsEmpty()
                }catch{
                    print("Error SwiftyJSON")
                }
            case .failure(let error):
                print("Error JSON: \(error)")
            }
        }
    
    
    }
    private func checkArrayIsEmpty() {
        if paymentMethodsArray.isEmpty {
            let dialog = AZDialogViewController(title: "Formas de pago", message: "Lo sentimos. Por el momento no contamos con una forma de pago para este evento.")
            self.present(dialog, animated: false, completion: nil)
            
            dialog.addAction(AZDialogAction(title: "Buscar nuevo evento") { (dialog) -> (Void) in
                self.navigationController?.popToRootViewController(animated: true)
                dialog.dismiss()
            })
        } else {
            tableView.reloadData()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "paymentSegueToShow" {
            if let fVC = segue.destination as? DeliveryTicketsVC {
                fVC.idZona = idZona
                fVC.valueEventoFuncion = valueEventoFuncion
                fVC.countTickets = countTickets
                fVC.seccion = seccion
                fVC.precio = precio
                fVC.nombre_evento = nombre_evento
                fVC.fecha_evento = fecha_evento
                fVC.hora_evento = hora_evento
                fVC.total_asientos = total_asientos
                fVC.total_fila_asiento_id = total_fila_asiento_id
                fVC.metodo_de_pago = metodo_de_pago
                fVC.importe_fijo_metodo_pago = importe_fijo_metodo_pago
                fVC.costo_metodo_de_pago = costo_metodo_de_pago
                fVC.idMetodoPago = idMetodoPago
                fVC.index_numerado = index_numerado
                fVC.mensagesetFila = mensagesetFila
                fVC.mensagesetIniColumna = mensagesetIniColumna
                fVC.mensagesetFinColumna = mensagesetFinColumna
                fVC.idfila = idfila
                fVC.direccion_evento = direccion_evento
                fVC.url_header_image = url_header_image
                fVC.more_information = more_information
            }
        }
        
    }
}

extension PaymenthMethodsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentMethodsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension PaymenthMethodsVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        metodo_de_pago = paymentMethodsArray[indexPath.row].texto
        idMetodoPago = paymentMethodsArray[indexPath.row].idTipoPago
        
        costo_metodo_de_pago = Double(paymentMethodsArray[indexPath.row].porcentajeCargo)/100
        
        costo_metodo_de_pago = Double(precio) * costo_metodo_de_pago
        costo_metodo_de_pago = costo_metodo_de_pago * Double(countTickets)
        
        importe_fijo_metodo_pago = paymentMethodsArray[indexPath.row].importeFijo * countTickets
        
        print("\(Double(importe_fijo_metodo_pago) + costo_metodo_de_pago)")
        
        self.performSegue(withIdentifier: "paymentSegueToShow", sender: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: bandCellId, for: indexPath) as! BandCell
        
        cell.titleLabel.text = paymentMethodsArray[indexPath.row].texto
        cell.pictureImageView.image = datasImages[0]
        
        switch paymentMethodsArray[indexPath.row].texto {
            
        case "En Punto de Venta":
            cell.pictureImageView.image = datasImages[0]
        case "Tarjeta de Crédito":
            cell.pictureImageView.image = datasImages[2]
        case "Tarjeta de Débito":
            cell.pictureImageView.image = datasImages[2]
        case "Pay Pal":
            cell.pictureImageView.image = datasImages[1]
        case "OXXO":
            cell.pictureImageView.image = datasImages[3]
        default:
            print("algo anda mal")
        }
        
        return cell
    }
    
    
}

class BandCell: UITableViewCell {
    
    let cellView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.setCellShadow()
        return view
    }()
    
    let pictureImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Name"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor.darkGray
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    func setup() {
        
        backgroundColor = UIColor(r: 245, g: 245, b: 245)
        addSubview(cellView)
        cellView.addSubview(pictureImageView)
        cellView.addSubview(titleLabel)
        
        cellView.setAnchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 4, paddingLeft: 8, paddingBottom: 4, paddingRight: 8)
        
        pictureImageView.setAnchor(top: nil, left: cellView.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, width: 60, height: 40)
        pictureImageView.centerYAnchor.constraint(equalTo: cellView.centerYAnchor).isActive = true
        
        titleLabel.setAnchor(top: nil, left: pictureImageView.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 20, paddingBottom: 0, paddingRight: 20, height: 40)
        titleLabel.centerYAnchor.constraint(equalTo: pictureImageView.centerYAnchor).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
