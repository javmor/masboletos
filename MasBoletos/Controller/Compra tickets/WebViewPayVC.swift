//
//  WeviViewPayVC.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 10/5/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit
import WebKit

class WebiViewPayVC: UIViewController, WKUIDelegate, WKNavigationDelegate {

    var idZona = Int()
    var countTickets = Int()
    var valueEventoFuncion = Int()
    var seccion = String()
    var precio = Int()
    
    var fecha_evento = String()
    var hora_evento = String()
    var nombre_evento = String()
    var total_asientos = String()
    var total_fila_asiento_id = String()
    var metodo_de_pago = String()
    var metodo_de_entrega = String()
    
    var costo_metodo_de_pago = Double()
    var importe_fijo_metodo_pago = Int()
    var idfila = Int()
    var costo_metodo_de_entrega = Double()
    
    var idMetodoPago = Int()
    var idFormaEntrega = Int()
    
    var index_numerado = Int()
    var id_usuario = Int()
    var id_transaccion = Int()
    var id_fe = Int()
    var mensagesetIdZona: Any = ""
    var mensagesetFila = String()
    var mensagesetIniColumna = Int()
    var mensagesetFinColumna = Int()
    
    var totalTodo = Double()
    var webView = WKWebView()
    
    override func loadView() {
        
        let webConfiguration = WKWebViewConfiguration()
        print("putas")
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var aux_string = ""
        mensagesetFila = mensagesetFila.replacingOccurrences(of: " ", with: "")
        print("KHA ZTA PAZANDO")
        if idfila == 0 {
            aux_string = ""
        } else {
            aux_string = String(idfila)
            total_asientos = ""
        }
        if UserDefaults.standard.isLoggedIn(){
            id_usuario = Int(UserDefaults.standard.getUserID())
            print("\(id_usuario) webview puñeta")
        }
        
        let urlString = "https://www.masboletos.mx/masBoletosEnviaDatos.php"
        let myURL = URL(string: urlString)
        var request = URLRequest(url: myURL!)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let postString = "idevento=\(valueEventoFuncion)&numerado=\(index_numerado)&cantidad=\(countTickets)&cargoxservicio=\(costo_metodo_de_pago+costo_metodo_de_entrega)&zona=\(idZona)&idcliente=\(id_usuario)&formadepago=\(idMetodoPago)&txtformaentrega=\(idFormaEntrega)&importe=\(precio)&idfila=\(aux_string)&inicolumna=\(mensagesetIniColumna)&fincolumna=\(mensagesetFinColumna)&fila=\(mensagesetFila)&idfilafilaasiento=\(total_fila_asiento_id)&filaasientos=\(total_asientos)&totalfinal=\(totalTodo)&origen=APP"
        
        request.httpBody = postString.data(using: .utf8)
        
        webView.load(request)
        showHUD(progressLabel: "Cargando")
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Listo", style: UIBarButtonItem.Style.done, target: self, action: #selector(addTapped))
        self.navigationItem.leftBarButtonItem = newBackButton;
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func addTapped() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if let serverTrust = challenge.protectionSpace.serverTrust {
            completionHandler(.useCredential, URLCredential(trust: serverTrust))
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        dismissHUD(isAnimated: true)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        dismissHUD(isAnimated: true)
    }
}
