 //
//  PuntosVentaVC.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 10/7/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit
import McPicker
import AZDialogView
import Mapbox
import ASMapLauncher
 
class PuntosVentaVC: UIViewController {
    
    //MARK: -Properties
    @IBOutlet weak var txtFldEstados: McTextField!
    
    var estadosAllFeed = [Estados]()
    var nameEstados = [String]()
    var sellers: [Seller]?
    var estadoFeed = [EstadoInd]()
    
    lazy var mapView: MGLMapView = {
        let map = MGLMapView()
        map.delegate = self
        return map
    }()

    //MARK: -init
    override func viewDidLoad() {
        super.viewDidLoad()
        configureMap()
        
        let urlString = "https://www.masboletos.mx/appMasboletos/getEstados.php"
        Service.shared.fetchGenericData(urlString: urlString, method: .get) { (feed: [Estados]) in

            self.estadosAllFeed = feed
            feed.forEach({ (items) in
                self.nameEstados.append(items.estado)
            })
            self.configurePickerLabel()
        }
    }

    //MARK: Helpers
    fileprivate func configurePickerLabel() {
        let mcInputView = McPicker(data: [nameEstados])
        mcInputView.backgroundColor = .gray
        mcInputView.backgroundColorAlpha = 0.25
        txtFldEstados.inputViewMcPicker = mcInputView
        
        txtFldEstados.doneHandler = { [weak txtFldEstados] (selections,rowPosition) in
            txtFldEstados?.text = selections[0]!
            //let id = self.estadosAllFeed[rowPosition].idEstado
            let id = self.estadosAllFeed[rowPosition].idEstado
            self.statesForId(id: id)
        }
        txtFldEstados.selectionChangedHandler = { [weak txtFldEstados] (selections, componentThatChanged) in
            txtFldEstados?.text = selections[componentThatChanged]!
        }
        txtFldEstados.cancelHandler = { [weak txtFldEstados] in
            txtFldEstados?.text = ""
        }
        txtFldEstados.textFieldWillBeginEditingHandler = { [weak txtFldEstados] (selections) in
            if txtFldEstados?.text == "" {
                // Selections always default to the first value per component
                txtFldEstados?.text = selections[0]
            }
        }
    }
    
    fileprivate func configureMap() {
        view.backgroundColor = UIColor.colorWithHexString(hexStr: "F6F6F6")
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.styleURL = MGLStyle.lightStyleURL
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .followWithHeading
        view.addSubview(mapView)
    
        if #available(iOS 11.0, *) {
            mapView.anchor(top: txtFldEstados.bottomAnchor, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 8, right: 8))
        } else {
            mapView.anchor(top: txtFldEstados.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 8, right: 8))
        }
    }
    
    fileprivate func statesForId(id: String) {
        let urlString = "https://www.masboletos.mx/appMasboletos/getPuntosVentaxEstado.php?idestado=\(id)"
        Service.shared.fetchGenericData(urlString: urlString, method: .get) { (feed: [EstadoInd]) in
            
            if !feed.isEmpty {
                self.estadoFeed = feed
                feed.forEach({ (items) in
                    
                    guard let lat = Double(items.lat) else { return }
                    guard let lon = Double(items.lon) else { return }
                    
                    let marker = MGLPointAnnotation()
                    marker.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                    marker.title = "Punto de venta"
                    marker.subtitle = "+MasBoletos"
                    
                    self.mapView.addAnnotation(marker)
                    
                })
  
            }else{
                let dialog = AZDialogViewController(title: "Whoops!", message: "Por el momento no contamos con sucursales en este estado.")
                dialog.show(in: self)
            }
        }
    }
    
    fileprivate func fetchDatasForId(id_punto_venta: Int) {
        let urlString = "https://www.masboletos.mx/appMasboletos/getDatosPuntoVenta.php?idpuntoventa=\(id_punto_venta)"
    
        Service.shared.fetchGenericData(urlString: urlString, method: .get) { (sellerFeed: [Seller]) in
            self.sellers = sellerFeed
            print(sellerFeed)
        }
    }

}

extension PuntosVentaVC: MGLMapViewDelegate {
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
    func mapView(_ mapView: MGLMapView, tapOnCalloutFor annotation: MGLAnnotation) {
       print(annotation.hash)
    }
    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        guard annotation is MGLPointAnnotation else {
            return nil
        }
        
        let imageName = "logo_masboletos"
        
        // Use the image name as the reuse identifier for its view.
        let reuseIdentifier = imageName
        
        // For better performance, always try to reuse existing annotations.
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        // If there’s no reusable annotation view available, initialize a new one.
        if annotationView == nil {
            annotationView = CustomImageAnnotationView(reuseIdentifier: reuseIdentifier, image: UIImage(named: imageName)!)
            
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MGLMapView, rightCalloutAccessoryViewFor annotation: MGLAnnotation) -> UIView? {
        //let button = UIButton()
        //button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //button.setImage(#imageLiteral(resourceName: "logo_masboletos"), for: .normal)
        //button.imageView?.contentMode = .scaleAspectFit
        //return button
        return UIButton(type: .detailDisclosure)
    }
    
    func mapView(_ mapView: MGLMapView, annotation: MGLAnnotation, calloutAccessoryControlTapped control: UIControl) {
        
        mapView.deselectAnnotation(annotation, animated: false)
        let mapLauncher = ASMapLauncher()
        
        let mapApps = mapLauncher.getMapApps()
        let alertController = UIAlertController(title: "Escoja su aplicación para navegación", message: nil, preferredStyle: .actionSheet)
        
        for mapApp in mapApps {
            
            let coordsInit = mapView.userLocation?.coordinate
            let coordsPunto = annotation.coordinate
            
            let action = UIAlertAction(title: mapApp, style: .default) { action in
                let fromMapPoint = MapPoint(location: .init(latitude: coordsInit!.latitude, longitude: coordsInit!.longitude), name: "", address: "")
                let toMapPoint = MapPoint(location: .init(latitude: coordsPunto.latitude, longitude: coordsPunto.longitude), name: "", address: "")
                
                _ = mapLauncher.launchMapApp(MapApp(rawValue: mapApp)!, fromDirections: fromMapPoint, toDirection: toMapPoint)
            }
        
            alertController.addAction(action)
        
        }
        
        alertController.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
 }

 struct Estados: Decodable {
    let idEstado: String
    let estado: String
    let respuesta: Int
 }
 
 struct EstadoInd: Decodable {
    let lat: String
    let lon: String
    let idpuntoventa: Int
    let respuesta: Int
 }
 
 struct Seller: Decodable {
    let lat: String
    let lon: String
    let idpuntoventa: Int
    let nombre: String
    let domicilio: String
    let telefono: String
    let imagen: String
    let respuesta: String
 }

 class CustomImageAnnotationView: MGLAnnotationView {
    var imageView: UIImageView!
    
    required init(reuseIdentifier: String?, image: UIImage) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        self.imageView = UIImageView(image: image)
        self.imageView.contentMode = .scaleAspectFit
        self.addSubview(self.imageView)
        self.imageView.centerInSuperview(size: .init(width: 60, height: 60))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
 }
