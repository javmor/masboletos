//
//  MuestreoEventosPorPaquetesVC.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/13/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MuestreoEventosPorPaquetesVC: BottomPopupViewController {

    let closeView: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Cerrar", for: .normal)
        btn.addTarget(self, action: #selector(handleClose(_:)), for: .touchUpInside)
        return btn
    }()
    
    let titlelbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "EVENTOS"
        lbl.font = UIFont(name: "AvenirNext-Medium", size: 15)
        lbl.textAlignment = .left
        return lbl
    }()
    
    var cvEventosPaquetes: UICollectionView!
    
    let identifier = "eventosLista"
    var id_eventPack = Int()
    
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    
    struct EventosPorPaquetes {
        let imagenes: String
        let informacion: String
    }
    var array_datasPaquetes = [EventosPorPaquetes]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
        view.backgroundColor = UIColor.colorWithHexString(hexStr: "F6F6F6")
    }
    
    override func getPopupHeight() -> CGFloat {
        return height ?? CGFloat(300)
    }
    
    override func getPopupTopCornerRadius() -> CGFloat {
        return topCornerRadius ?? CGFloat(10)
    }
    
    override func getPopupPresentDuration() -> Double {
        return presentDuration ?? 1.0
    }
    
    override func getPopupDismissDuration() -> Double {
        return dismissDuration ?? 1.0
    }
    
    override func shouldPopupDismissInteractivelty() -> Bool {
        return shouldDismissInteractivelty ?? true
    }
    
    private func setup() {
        //cvEventosPaquetes
        
        setupCollectionView()
        
        [closeView, titlelbl, cvEventosPaquetes].forEach { view.addSubview($0) }
        
        titlelbl.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: closeView.leadingAnchor, padding: .init(top: 4, left: 13, bottom: 0, right: 0), size: .init(width: 0, height: 30))
        
        closeView.anchor(top: view.topAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 4, left: 0, bottom: 0, right: 13), size: .init(width: 0, height: 30))
        
        cvEventosPaquetes.anchor(top: titlelbl.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(equalInsets: 0))
        downloadEventosPorPaquetes()
    }
    
    @objc func handleClose(_ sender: UIButton) { dismiss(animated: true, completion: nil) }
    
    private func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: 350, height: 90)
        
        cvEventosPaquetes = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: layout)
        cvEventosPaquetes.register(UINib(nibName: "cellEventosLista", bundle: nil), forCellWithReuseIdentifier: identifier)
        cvEventosPaquetes.delegate = self
        cvEventosPaquetes.dataSource  = self
        cvEventosPaquetes.backgroundColor = UIColor.clear
    }
    
    private func downloadEventosPorPaquetes() {
        let urlString = "https://www.masboletos.mx/appMasboletos/getImgEventosPack.php?idEventoPack=\(id_eventPack)"
        
        Alamofire.request(urlString).responseJSON {
            response in
            switch response.result {
            case .success:
                do{
                    let datas_json = try JSON(data: response.data!)
                    let datas = datas_json.arrayValue
                    
                    for elements in datas {
                        
                        let urlImage = elements["imagenes"].stringValue
                        if  !urlImage.isEmpty {
                            var imagenes = "https://www.masboletos.mx/sica/imgEventos/\(urlImage)"
                            let informacion = elements["informacion"].stringValue
                            imagenes = imagenes.replacingOccurrences(of: " ",with: "%20")
                            self.array_datasPaquetes.append( EventosPorPaquetes(imagenes: imagenes, informacion: informacion))
                        }else{
                            var imagenes = "https://www.masboletos.mx/sica/imgEventos/banner17.png"
                            let informacion = elements["informacion"].stringValue
                            imagenes = imagenes.replacingOccurrences(of: " ",with: "%20")
                            self.array_datasPaquetes.append( EventosPorPaquetes(imagenes: imagenes, informacion: informacion))
                        }
                    

                    }
                    self.array_datasPaquetes = self.array_datasPaquetes.sorted(by: { $0.informacion < $1.informacion })
                    self.cvEventosPaquetes.reloadData()
                    
                }catch{
                    print("Putas pa todos")
                }
                
            case .failure(let error):
                print("Error \(error)")
            }
        }
        
    }
    
}

extension MuestreoEventosPorPaquetesVC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array_datasPaquetes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! EventosListaCell
        
        cell.title.text = array_datasPaquetes[indexPath.row].informacion
        cell.title.adjustsFontSizeToFitWidth = true
        
        if array_datasPaquetes[indexPath.row].imagenes.isEmpty {
            cell.imgEvento.image = UIImage(named: "logo_masboletos")
        }else{
            cell.imgEvento.sd_setImage(with: URL(string:  array_datasPaquetes[indexPath.row].imagenes), placeholderImage: UIImage(named: "logo_masboletos"))
        }
        cell.cellView.setCardView(view: cell.cellView)
        return cell
    }

}
