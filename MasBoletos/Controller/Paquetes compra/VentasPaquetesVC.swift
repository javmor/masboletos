//
//  VentasPaquetesVC.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/12/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ViewAnimator
import SDWebImage

class VentasPaquetesVC: UIViewController {
    
    var cvPaquetes: UICollectionView!
    let identifier = "paquetesCell"
 
    var array_paquetes = [PaquetesOrganizadoresModel]()
 
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor(red: 5.0/255, green: 3.0/255, blue: 73.0/255, alpha: 1.0)
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        return refreshControl
    }()
    private var animations = [AnimationType.from(direction: .bottom, offset: 50.0)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        downloadPackagesEvents()
    }
    
    private func setup() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: 350, height: 70)
        
        cvPaquetes = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: layout)
        cvPaquetes!.register(UINib(nibName: "CellPaquetes", bundle: nil), forCellWithReuseIdentifier: identifier)
        cvPaquetes!.delegate = self
        cvPaquetes!.dataSource = self
        cvPaquetes.backgroundColor = UIColor.colorWithHexString(hexStr: "F6F6F6")
        view.addSubview(cvPaquetes!)
        cvPaquetes!.fillSuperview()
        setRefreshCollectionView()
    }
    
    private func downloadPackagesEvents() {
        let urlString = "https://www.masboletos.mx/appMasboletos/getPaquetesOrganizador.php"
        
        Alamofire.request(urlString).responseJSON {
            response in
            
            switch response.result {
            case.success:
                do{
                    let json = try JSON(data: response.data!)
                    let datas = json.arrayValue
                    for elements in datas {
                        let idorganizador = elements["idorganizador"].intValue
                        let nombre = elements["nombre"].stringValue
                        let banner = "https://www.masboletos.mx/sica/imgEventos/\(elements["banner"].stringValue)"
                        let domicilio = elements["domicilio"].stringValue
                        
                        self.array_paquetes.append(PaquetesOrganizadoresModel(idorganizador: idorganizador, nombre: nombre, banner: banner, domicilio: domicilio))
                    }
                    self.cvPaquetes.reloadData()
                }catch{
                    print("Error")
                }
                break
            case .failure(let error):
                print("Error: \(error)")
                break
            }
        }
    }
    
    private func setRefreshCollectionView() {
        cvPaquetes.alwaysBounceVertical = true
        if #available(iOS 10.0, *) { cvPaquetes.refreshControl = refresher }
        else{ cvPaquetes.addSubview(refresher) }
    
    }
    
    @objc func refresh(sender:AnyObject) {
        
        let deadline = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.refresher.endRefreshing()
            self.cvPaquetes.reloadData()
            self.cvPaquetes.performBatchUpdates({
                UIView.animate(views: self.cvPaquetes.orderedVisibleCells,
                               animations: self.animations, completion: {
                                
                })
            }, completion: nil)
        }
    }
}

extension VentasPaquetesVC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array_paquetes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! PaquetesCollectionCell
        
        cell.imgEvento.sd_setImage(with: URL(string: array_paquetes[indexPath.row].banner), placeholderImage: UIImage(named: "logo_masboletos"))
        cell.name_evento.text = array_paquetes[indexPath.row].nombre
        cell.viewCell.setCardView(view: cell.viewCell)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let muestraPaquetes = storyboard.instantiateViewController(withIdentifier: "PaquetesMuestraVC") as! PaquetesMuestraVC
        muestraPaquetes._idOrganizador = array_paquetes[indexPath.row].idorganizador
        navigationController?.pushViewController(muestraPaquetes, animated: true)
    }
}
