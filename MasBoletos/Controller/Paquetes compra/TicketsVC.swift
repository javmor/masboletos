//
//  TicketsVC.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/15/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import VisualEffectView

class TicketsVC: UIViewController {

    @IBOutlet weak var imgEvento: UIImageView!
    @IBOutlet weak var imgLeftEvento: UIImageView!
    
    @IBOutlet weak var title_evento: UILabel!
    @IBOutlet weak var subTitle_evento: UILabel!
    @IBOutlet weak var information_evento: UILabel!
    
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var count_tickets: UILabel!
    
    //MARK: Structs
    
    struct EventoInformation {
        let imagen: String
        let nombre: String?
        let descripcion: String?
    }
    
    //MARK: Arrays
    var array_eventoInfo = [EventoInformation]()
    
    let nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.addSettingsButtonBarNextEnabled()
        return button
    }()
    
    //MARK: Variables by getting informartion for event pack
    
    var _countTickets = String()
    var _idEventPack = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        downloadInformationEvent()
        setup()
    }
    
    @IBAction func changeStepper(_ sender: UIStepper) {
        
        count_tickets.text = "\(Int(sender.value))"
        _countTickets = "\(Int(sender.value))"
        if sender.value == 0 {
            nextButton.setDisabledButtonBarNext()
        }else{
            nextButton.setEnabledButtonBarNext()
        }
    }
    
    private func setup() {
        nextButton.setDisabledButtonBarNext()
        let buttonNavBarItem = UIBarButtonItem.init(customView: nextButton)
        nextButton.addTarget(self, action: #selector(onButtonNextView(_:)), for: .touchUpInside)
        navigationItem.rightBarButtonItems = [buttonNavBarItem]
        navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    @objc func onButtonNextView(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let seccionVC = storyboard.instantiateViewController(withIdentifier: "SeccionVC") as! SeccionVC
        seccionVC._countTickets = _countTickets
        seccionVC._idEventPack = _idEventPack 
        navigationController?.pushViewController(seccionVC, animated: true)
        
    }
    
    private func downloadInformationEvent() {
        let urlString = "https://www.masboletos.mx/appMasboletos/getPaqueteEncabezado.php?IdEventoPack=\(_idEventPack)"
        
        Alamofire.request(urlString).responseJSON {
            response in
            
            switch response.result {
            case .success:
                do{
                    let json_data = try JSON(data: response.data!)
                    let datas = json_data.arrayValue
                    print(json_data)
                    for elements in datas {
                        let imagen = elements["imagen"].stringValue
                        let nombre = elements["nombre"].stringValue
                        let descripcion = elements["descripcion"].stringValue
                        self.array_eventoInfo.append(EventoInformation(imagen: imagen, nombre: nombre, descripcion: descripcion))
                    }
                    self.setBlurViewInformation()
                }catch{
                    print("Error en la wea")
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    private func setBlurViewInformation() {
        title_evento.text = array_eventoInfo[0].nombre ?? "N/D"
        title_evento.numberOfLines = 0
        title_evento.sizeToFit()
        
        subTitle_evento.text = array_eventoInfo[0].descripcion ?? "N/D"
        subTitle_evento.numberOfLines = 2
        subTitle_evento.sizeToFit()
        
        let image = removeSpace(str: array_eventoInfo[0].imagen)
        
        if !image.isEmpty {
            self.imgLeftEvento.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "logo_masboletos"))
            self.imgEvento.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "logo_masboletos"))
        }else{
            self.imgLeftEvento.image = UIImage(named: "logo_masboletos")
            self.imgLeftEvento.contentMode = .scaleAspectFit
            self.imgEvento.contentMode = .scaleAspectFit
            self.imgEvento.image = UIImage(named: "logo_masboletos")
        }
        
        let visualEffectView = VisualEffectView(frame: CGRect(x: 0, y: 0, width: imgEvento.frame.width, height: imgLeftEvento.frame.height + 5))
        
        visualEffectView.colorTint = .black
        visualEffectView.colorTintAlpha = 0.2
        visualEffectView.blurRadius = 10
        visualEffectView.scale = 1
        
        self.imgEvento.addSubview(visualEffectView)
    }
}
