//
//  SeccionVC.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/15/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import McPicker

class SeccionVC: UIViewController {
    
    var _countTickets = String()
    var _idEventPack = Int()
    
    var array_zonasPaquetes = [PaquetesZonasModel]()
    var array_textZonas = [String]()
    
    var array_seccionPaquetes = [PaquetesSeccionModel]()
    var array_textSeccion = [String]()
    
    var array_seccion = [ModelSeccion]()
    var array_betterPlace = [ModelMejorDisponiblePaquete]()
    
    let nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.addSettingsButtonBarNextEnabled()
        return button
    }()
    
    //MARK: Properties
    let imgRecinto: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.image = UIImage(named: "logo_masboletos")
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    //MARK: Buttons zonas and label
    let btnZonas: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.backgroundColor = UIColor.colorWithHexString(hexStr: "050349")
        btn.titleLabel?.font = UIFont(name: "AvenirNext-Regular", size: 15)
        btn.addTarget(self, action: #selector(handleZonas(_:)), for: .touchUpInside)
        btn.addBorderToButton(cRadius: 20, bWidth: 1)
        btn.setTitle("Seleccione su zona", for: .normal)
        btn.tag = 1
        return btn
    }()
    
    let zonas_info: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "AvenirNext-Medium", size: 13)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    //MARK: Buttons sections and label
    let btnSeccion: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Seleccione su sección", for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.backgroundColor = UIColor.colorWithHexString(hexStr: "050349")
        btn.titleLabel?.font = UIFont(name: "AvenirNext-Regular", size: 15)
        btn.addTarget(self, action: #selector(handleZonas(_:)), for: .touchUpInside)
        btn.addBorderToButton(cRadius: 20, bWidth: 1)
        btn.tag = 2
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let seccion_info: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "AvenirNext-Medium", size: 13)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    //MARK: Label and switch
    
    let title_switch: UILabel = {
        let lbl = UILabel()
        lbl.text = "Mostrar mapa de asientos"
        lbl.font = UIFont(name: "AvenirNext-Medium", size: 13)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.isHidden = true
        return lbl
    }()
    
    let switchSetSeat: UISwitch = {
        let sw = UISwitch()
        sw.onTintColor = UIColor.colorWithHexString(hexStr: "050349")
        sw.translatesAutoresizingMaskIntoConstraints = false
        sw.isOn = false
        sw.setOn(false, animated: true)
        sw.isHidden = true
        sw.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)
        return sw
    }()
    
    //MARK: init
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    //MARK: Selectors
    
    @objc func onButtonNextView(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let seccionVC = storyboard.instantiateViewController(withIdentifier: "sbSellThird") as! SeatSelectVC
        navigationController?.pushViewController(seccionVC, animated: true)
    }
    
    @objc func switchValueDidChange(_ sender: UISwitch) {
        if sender.isOn {
            print("Estoy ON perra")
        }else{
            print("Estoy OFF perra")
        }
    }
    
    @objc func handleZonas(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            downloadsZonasTickets()
        case 2:
            setBottomPickerWithSeccion()
        default:
            break
        }
    }
    
    //MARK: Helpers
    
    private func setup() {
        nextButton.setDisabledButtonBarNext()
        let buttonNavBarItem = UIBarButtonItem.init(customView: nextButton)
        nextButton.addTarget(self, action: #selector(onButtonNextView(_:)), for: .touchUpInside)
        navigationItem.rightBarButtonItems = [buttonNavBarItem]
        navigationController?.navigationBar.shadowImage = UIImage()
        title = "ASIENTOS"
        
        [imgRecinto, btnZonas, zonas_info, btnSeccion, seccion_info, title_switch, switchSetSeat].forEach { view.addSubview($0) }
        
        imgRecinto.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imgRecinto.topAnchor.constraint(equalTo: view.topAnchor, constant: 8).isActive = true
        imgRecinto.widthAnchor.constraint(equalToConstant: 350).isActive = true
        imgRecinto.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.4).isActive = true
        
        btnZonas.anchor(top: imgRecinto.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 40))
        
        zonas_info.anchor(top: btnZonas.bottomAnchor, leading: btnZonas.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 34))
        
        btnSeccion.anchor(top: zonas_info.bottomAnchor, leading: btnZonas.leadingAnchor, bottom: nil, trailing: btnZonas.trailingAnchor, padding: .init(top: 8, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 40))
        
        seccion_info.anchor(top: btnSeccion.bottomAnchor, leading: btnZonas.leadingAnchor, bottom: nil, trailing: btnSeccion.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 34))
        
        title_switch.anchor(top: seccion_info.bottomAnchor, leading: btnZonas.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 8, left: 0, bottom: 35, right: 0),size: .init(width: 0, height: 34))
        
        switchSetSeat.anchor(top: title_switch.topAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 32), size: .init(width: 0, height: 30))
        
    }
    
    private func downloadsZonasTickets() {
        let urlString  = "https://www.masboletos.mx/appMasboletos/getPaquetesZonas.php?idpaquete=\(_idEventPack)"
        
        array_zonasPaquetes.removeAll()
        array_textZonas.removeAll()
        Alamofire.request(urlString).responseJSON { response in
            switch response.result {
            case .success:
                
                do{
                    let json = try JSON(data: response.data!)
                    let datas = json.arrayValue
                    
                    print(json)
                    for item in datas {
                        let zona = item["zona"].stringValue
                        let numerado = item["numerado"].intValue
                        let comision = item["comision"].stringValue
                        let precio = item["precio"].stringValue
                        
                        self.array_zonasPaquetes.append(PaquetesZonasModel(zona: zona, numerado: numerado, comision: comision, precio: precio))
                        let complete = " \(zona) \(self.convertDoubleToCourrency(amount: Double(precio)!)) c/u"
                        self.array_textZonas.append(complete)
                    }
                    self.setBottomPickerWithZonas()
                    
                }catch{
                    print("Error SwiftyJSON")
                }
                
                
            case .failure(let error):
                print("Error: \(error)")
            }
        }
    }
    
    private func setBottomPickerWithZonas() {
        McPicker.show(data: [array_textZonas]) {  [weak self] (selections: [Int : String], rowPosition: Int) -> Void in
            
            if let name = selections[0] {
                self?.zonas_info.text = name
                self!.downloadsSeccionsTickets(_grupo: self!.array_zonasPaquetes[rowPosition].zona)
                self!.getBetterPlacePaquetes(_numerado: self!.array_zonasPaquetes[rowPosition].numerado, _grupo: self!.array_zonasPaquetes[rowPosition].zona)
            }
        }
        
    }
    
    private func setBottomPickerWithSeccion() {
        McPicker.show(data: [array_textSeccion]) {  [weak self] (selections: [Int : String], rowPosition: Int) -> Void in
            
            if let name = selections[0] {
                self?.seccion_info.text = name
                
                if name == "MEJOR SELECCIÓN" {
                    print("mejor seleccion")
                }
            }
        }
    }
    
    private func downloadsSeccionsTickets(_grupo: String) {
        let urlString  = "https://www.masboletos.mx/appMasboletos/getCargandoSubzonasxGrupoPaquete.php?idpaquete=\(_idEventPack)&grupo=\(_grupo)"
        
        array_seccionPaquetes.removeAll()
        array_textSeccion.removeAll()
        array_textSeccion.append("MEJOR SELECCIÓN")
        Alamofire.request(urlString).responseJSON{ response in
            switch response.result {
            case .success:
                
                do{
                    let json = try JSON(data: response.data!)
                    let datas = json.arrayValue
                    
                    for item in datas {
                        let value = item["value"].intValue
                        let descripcion = item["descripcion"].stringValue
                        
                        self.array_seccionPaquetes.append(PaquetesSeccionModel(value: value, descripcion: descripcion))
                        self.array_textSeccion.append(descripcion)
                    }
                }catch{
                    print("Error SwiftyJSON")
                }
            case .failure(let error):
                print("Error: \(error)")
            }
        }
        
    }
    
    
    private func getBetterPlacePaquetes(_numerado: Int, _grupo: String) {

        let urlString = "https://www.masboletos.mx/appMasboletos/getBoletosPaquete.php?idpaquete=\(_idEventPack)&numerado=\(_numerado)&grupo=\(_grupo)&cantBoletos=\(_countTickets)"
        
        print(urlString)
        Alamofire.request(urlString).responseJSON { response in
            switch response.result {
            case .success:
                do{
                    let json = try JSON(data: response.data!)
                    
                    let dataEvento = json["dataEvento"].arrayValue

                    for itemEvento in dataEvento {
                        let mensagesetTipo = itemEvento["mensagesetTipo"].intValue
                        let mensagesetMensage = itemEvento["mensagesetMensage"].stringValue
                        let mensagesetDescripcion = itemEvento["mensagesetDescripcion"].stringValue
                        let mensagesetAsientos = itemEvento["mensagesetAsientos"].stringValue
                        let mensagesetIdZona = itemEvento["mensagesetIdZona"].intValue
                        let disponibles = itemEvento["disponibles"].stringValue
                        let mensagesetIdPuerta = itemEvento["mensagesetIdPuerta"].intValue
                        let aforo = itemEvento["aforo"].stringValue
                        let vendidos = itemEvento["vendidos"].stringValue
                        let precio = itemEvento["precio"].stringValue
                        let idevento = itemEvento["idevento"].intValue
                        let mensagesetFila = itemEvento["mensagesetFila"].stringValue
                        let mensagesetImporteComision = itemEvento["mensagesetImporteComision"].intValue
                        let mensagesetNumerado = itemEvento["mensagesetNumerado"].intValue
                        let mensagesetIniColumna = itemEvento["mensagesetIniColumna"].intValue
                        let mensagesetFinColumna = itemEvento["mensagesetFinColumna"].intValue
                        let idfila = itemEvento["idfila"].intValue
                        
                        self.array_betterPlace.append(ModelMejorDisponiblePaquete(mensagesetTipo: mensagesetTipo, mensagesetMensage: mensagesetMensage, mensagesetDescripcion: mensagesetDescripcion, mensagesetAsientos: mensagesetAsientos, mensagesetIdZona: mensagesetIdZona, disponibles: disponibles, mensagesetIdPuerta: mensagesetIdPuerta, aforo: aforo, vendidos: vendidos, precio: precio, idevento: idevento, mensagesetFila: mensagesetFila, mensagesetImporteComision: mensagesetImporteComision, mensagesetNumerado: mensagesetNumerado, mensagesetIniColumna: mensagesetIniColumna, mensagesetFinColumna: mensagesetFinColumna, idfila: idfila))
                    }
                    
                } catch {
                    print("Error JSON")
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
}
