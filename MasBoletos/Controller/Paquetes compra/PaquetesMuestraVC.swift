//
//  PaquetesMuestraVC.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 2/13/19.
//  Copyright © 2019 IT-STAM. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PaquetesMuestraVC: UIViewController {
    
    
    var cvEventosPack: UICollectionView!

    let identifier = "eventosPaquetes"
    var _idOrganizador = Int()
    
    var array_eventosPaquetes = [PaquetesMuestraModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        title = "Venta de paquetes"
        view.backgroundColor = UIColor.colorWithHexString(hexStr: "F6F6F6")
        setupCollectionView()
        [cvEventosPack].forEach { view.addSubview($0) }
        cvEventosPack.fillSuperview()
        navigationController?.isToolbarHidden = true
        view.backgroundColor = UIColor.colorWithHexString(hexStr: "F6F6F6")
        downloadInformationFromAPI()
    }
    
    private func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: 350, height: 122)
        
        cvEventosPack = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: layout)
        cvEventosPack.register(UINib(nibName: "CellEventosDisponibles", bundle: nil), forCellWithReuseIdentifier: identifier)
        cvEventosPack.delegate = self
        cvEventosPack.dataSource  = self
        cvEventosPack.backgroundColor = UIColor.clear
    }
    
    @objc func handleTap(_ sender: UIButton) {
        guard let popupVC = storyboard?.instantiateViewController(withIdentifier: "MuestraEventosPaquetes") as? MuestreoEventosPorPaquetesVC else { return }
        
        popupVC.id_eventPack = array_eventosPaquetes[sender.tag].idEventPack
        let part = view.bounds.height/4
        popupVC.height = part*3
        popupVC.topCornerRadius = 10.12
        popupVC.presentDuration = 0.30
        popupVC.dismissDuration = 0.30
        popupVC.shouldDismissInteractivelty = true
        present(popupVC, animated: true, completion: nil)
    }
    
    private func downloadInformationFromAPI() {
        let urlString = "https://www.masboletos.mx/appMasboletos/getPaquetesOrganizador_detalle.php?idorganizador=\(_idOrganizador)"
        
        Alamofire.request(urlString).responseJSON {
            response in
            
            switch response.result {
            case .success:
                
                do {
                    let _jsonDatas = try JSON(data: response.data!)
                    
                    let datas = _jsonDatas.arrayValue
                    for elements in datas {
                        let imagen = elements["imagen"].stringValue
                        let nombre = elements["nombre"].stringValue
                        let precio = elements["precio"].stringValue
                        let idEventPack = elements["IdEventoPack"].intValue
                        self.array_eventosPaquetes.append(PaquetesMuestraModel(imagen: imagen, nombre: nombre, precio: precio, idEventPack: idEventPack))
                    }
                    self.cvEventosPack.reloadData()
                }catch{
                    print("Error JSON")
                }
            case .failure(let error):
                print("Error \(error)")
            }
        }
    }
    
    private func setCurrency(pago: Int) -> String {
        let pago_formatter = pago
        var aux:String = ""
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "es_MX")
        formatter.numberStyle = .currency
        
        if let formattedTipAmount = formatter.string(from: pago_formatter as NSNumber) {
            aux = formattedTipAmount
        }
        
        return aux
    }
    

    
}

extension PaquetesMuestraVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array_eventosPaquetes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! EventosPaquetesCell
        //cell.imgEvento.image = UIImage(named: "logo_masboletos")
        cell.title.text = array_eventosPaquetes[indexPath.row].nombre
        cell.price.text = "\(convertDoubleToCourrency(amount: Double(array_eventosPaquetes[indexPath.row].precio)!))"
        cell.showEventos.addTarget(self, action: #selector(handleTap(_:)), for: .touchUpInside)
        cell.showEventos.tag = indexPath.row
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tickets = storyboard.instantiateViewController(withIdentifier: "Tickets") as! TicketsVC
        tickets._idEventPack = array_eventosPaquetes[indexPath.row].idEventPack
        navigationController?.pushViewController(tickets, animated: true)
    }
    
}
