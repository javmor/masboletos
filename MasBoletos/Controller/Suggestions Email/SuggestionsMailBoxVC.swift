//
//  SuggestionsMailBoxVC.swift
//  MasBoletos
//
//  Created by Victor Javier Arroyo Morales on 10/12/18.
//  Copyright © 2018 IT-STAM. All rights reserved.
//

import UIKit
import Eureka
import Alamofire
import AZDialogView

class SuggestionsMailBoxVC: FormViewController {

    
    var jsonArray: NSArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Buzón de sugerencias"
        
        form +++ Section("Datos Personales")
            <<< TextRow("nombre"){ row in
                row.title = "Nombre"
                row.placeholder = "Escribe tu nombre aquí"
                
                let ruleRequiredViaClosure = RuleClosure<String> { rowValue in
                    return (rowValue == nil || rowValue!.isEmpty) ? ValidationError(msg: "Field required!") : nil
                }
                row.add(rule: ruleRequiredViaClosure)
                
                
                row.validationOptions = .validatesAlways
                
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        cell .titleLabel?.textColor = .red
                    }
                    
                }
            <<< EmailRow("correo"){ row in
                row.title = "Correo electrónico"
                row.placeholder = "Correo electrónico"
                row.add(rule: RuleRequired())
                row.add(rule: RuleEmail())
                row.validationOptions = .validatesAlways
                
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
                }
                    
            <<< PhoneRow("telefono"){
                $0.title = "Teléfono"
                $0.placeholder = "Teléfono"
                
                
                $0.add(rule: RuleMaxLength(maxLength: 10))
                $0.validationOptions = .validatesOnChange
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                        
                    }
                }
            
            +++ Section("Ayúdanos a crecer")
            
            <<< ActionSheetRow<String>("tipomensaje") {
                $0.title = "Tipo de mensaje"
                $0.selectorTitle = "Escoge un mensaje"
                $0.options = ["Sugerencia","Queja"]
                $0.value = "Sugerencia"
              
            }
            <<< ActionSheetRow<String>("servicio") {
                $0.title = "Servicio"
                $0.selectorTitle = "Escoge un servicio"
                $0.options = ["Aplicación móvil","Página web"]
                $0.value = "Aplicación móvil"
                
            }
            <<< ActionSheetRow<String>("ciudad") {
                $0.title = "Estados"
                $0.selectorTitle = "Escoge un estado"
                $0.options = ["Aguascalientes","Baja California","Baja California Sur", "Campeche", "Chiapas", "Chihuahua","Coahuila","Colima","CDMX","Durango","Guanajuato"
                ,"Guerrero","Hidalgo","Jalisco","México","Michoacán","Morelos","Nayarit","Nuevo León","Oaxaca","Puebla","Querétaro","Quintana Roo","San Luis Potosí","Sinaloa"
                ,"Sinaloa","Sonora","Tabasco","Tamaulipas","Tlaxcala","Veracruz","Yucatán","Zacatecas"]
                $0.value = "Querétaro"
            }
            <<< TextAreaRow("mensaje"){ txtRow in
                txtRow.title = "Comentarios"
                txtRow.placeholder = "Escribe tus comentarios"
                txtRow.add(rule: RuleRequired())
                txtRow.validationOptions = .validatesAlways
                }.cellUpdate({ (cell, row) in
                    if !row.isValid {
                        cell.textView?.textColor = .red
                        
                    }
                })
        
            +++ Section("")
            <<< ButtonRow(){ btn in
                btn.title = "Enviar"
               
                }.onCellSelection({ (cell, row) in

                    if  row.section?.form?.validate().count != 0 {
                        let dialog = AZDialogViewController(title: "Error", message: "Faltan algunos campos")
                        self.present(dialog, animated: false, completion: nil)
                        
                    } else {
                        print("true")
                    
                        let row: TextRow? = self.form.rowBy(tag: "nombre")
                        let value_nombre = row!.value
                        
                        let row2: EmailRow? = self.form.rowBy(tag: "correo")
                        let value_correo = row2!.value
                        
                        let row3: PhoneRow? = self.form.rowBy(tag: "telefono")
                        let value_telefono = row3!.value
                        
                        let row4: RowOf<String>! = self.form.rowBy(tag: "tipomensaje")
                        let value_tipomensaje = row4.value!
                        
                        let row5: RowOf<String>! = self.form.rowBy(tag: "servicio")
                        let value_servicio = row5.value!
                        
                        let row6: RowOf<String>! = self.form.rowBy(tag: "ciudad")
                        let value_ciudad = row6.value!
                        
                        let row7: TextAreaRow? = self.form.rowBy(tag: "mensaje")
                        let value_mensaje = row7?.value
                        
                        let params: [String : String] = [
                            "nombre": value_nombre!,
                            "correo": value_correo!,
                            "telefono": value_telefono!,
                            "tipomensaje": value_tipomensaje,
                            "servicio": value_servicio,
                            "ciudad": value_ciudad,
                            "mensaje": value_mensaje!
                        ]
                        self.showHUD(progressLabel: "Enviando")
                        self.sendInformation(params: params)
                    }
                })
        
    }
    
    
    //https://www.masboletos.mx/mailAPP.php
    
    func sendInformation(params: [String:String]) {
        let urlString = "https://www.masboletos.mx/mailAPP.php"

        Alamofire.request(urlString, method: .post, parameters: params,encoding: URLEncoding.httpBody, headers: nil ).responseJSON {
            response in
            
            switch response.result {
            case .success:
                
                let respuesta = response.result.value
                print(respuesta!)
                
                self.dismissHUD(isAnimated: true)
                let dialog = AZDialogViewController(title: "¡Gracias por tu ayuda!", message: "Tu mensaje se ha enviado correctamente")
                
                dialog.addAction(AZDialogAction(title: "Regresar") { (dialog) -> (Void) in
                    
                    self.navigationController?.popToRootViewController(animated: true)
                    dialog.dismiss()
                })
                
                self.present(dialog, animated: false, completion: nil)
                
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
    }
    
}


